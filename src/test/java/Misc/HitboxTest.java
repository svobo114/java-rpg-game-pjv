
package Misc;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jan Svoboda-Fel-CVUT
 */
public class HitboxTest {
    private static final int _SQUARE=1000;
    Hitbox hbA;
    Hitbox hbB; 
    Hitbox hbC;  
    Coord pointA;
    Coord pointB;
    Coord pointC;
    Coord pointD;
    Coord pointE;
    Coord pointF;
    
    public HitboxTest() {
        hbA=new Hitbox(); //empty means -1,-1
        hbB=new Hitbox(new Coord(0,0), _SQUARE, _SQUARE, 100, 100); 
        hbC=new Hitbox(new Coord(50,50), _SQUARE, _SQUARE, 100, 100); 
        pointA=new Coord(0,0);
        pointB=new Coord(100,0);
        pointC=new Coord(-100,0);
        pointD=new Coord(0,100);
        pointE=new Coord(1500,-1500);
        pointF=new Coord(-1,-1);
    }
    
    private boolean compare(Hitbox a, Hitbox b){
        return a.leftUp.equals(b.leftUp) && a.rightDown.equals(b.rightDown);
    }

    /**
     * Test of isIn method, of class Hitbox.
     */
    @Test
    public void testIsIn() {
        
        assertFalse("Point A is Supposed to be out",hbA.isIn(pointA));
        assertFalse("Point B is Supposed to be out",hbA.isIn(pointB));
        assertFalse("Point C is Supposed to be out",hbA.isIn(pointC));
        assertFalse("Point D is Supposed to be out",hbA.isIn(pointD));
        assertFalse("Point E is Supposed to be out",hbA.isIn(pointE));
        assertTrue( "Point F is Supposed to be in",hbA.isIn(pointF));
        
        assertTrue("Point A is Supposed to be in",hbB.isIn(pointA));
        assertTrue("Point B is Supposed to be in",hbB.isIn(pointB));
        assertFalse("Point C is Supposed to be out",hbB.isIn(pointC));
        assertTrue("Point D is Supposed to be in",hbB.isIn(pointD));
        assertFalse("Point E is Supposed to be out",hbB.isIn(pointE));
        assertFalse( "Point F is Supposed to be out",hbB.isIn(pointF));

    }

    /**
     * Test of doesCollide method, of class Hitbox.
     */
    @Test
    public void testDoesCollide() {
        //with each other
        assertFalse("hb A and B should not collide",hbA.doesCollide(hbB));
        assertFalse("hb A and C should not collide",hbA.doesCollide(hbC));
        assertTrue("hb C and B should collide",hbC.doesCollide(hbB));
        
        //with itself
        assertTrue("hb should collide with itself",hbC.doesCollide(hbC));
        assertTrue("hb should collide with itself, this is -1,-1",hbA.doesCollide(hbA));
        
    }

    /**
     * Test of isInRange method, of class Hitbox.
     */
    @Test
    public void testIsInRange() {
      assertTrue("Coord is just next",hbA.isInRange(pointA, 50)); //true
      assertTrue("Coord is on the edge",hbB.isInRange(pointC, 100)); //true
      assertFalse("Coord is really far",hbB.isInRange(pointE, 100)); //false
    }

    /**
     * Test of returnCopy method, of class Hitbox.
     */
    @Test
    public void testReturnCopy() {
        Hitbox tmp=hbB.returnCopy();
        assertTrue("tmp is a copy",compare(tmp,hbB));
    }

    /**
     * Test of copyCoords method, of class Hitbox.
     */
    @Test
    public void testCopyCoords() {
        Hitbox tmp=new Hitbox(); //-1,-1
        tmp.copyCoords(hbA);
        assertTrue("Should have the same coords",compare(tmp,hbA));
    }

    /**
     * Test of setX method, of class Hitbox.
     */
    @Test
    public void testSetX() {
        //set X to something and then compare
        hbC.setX(200);
        assertTrue(compare(hbC,new Hitbox(new Coord(200,50),100,100)));
    }

    /**
     * Test of setY method, of class Hitbox.
     */
    @Test
    public void testSetY() {
        //set Y to something and then compare
        hbC.setY(200); //x,y=50,50 and w,h =100,100 ==> set x,y=50,200
        assertTrue(compare(hbC,new Hitbox(new Coord(50,200),100,100)));
    }
    
}
