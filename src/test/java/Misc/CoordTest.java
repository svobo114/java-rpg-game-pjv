
package Misc;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test class for CoordTest
 * @author Jan Svoboda-Fel-CVUT
 */
public class CoordTest {
    Coord a;
    Coord b;
    Coord c;
    Coord d;
    Coord e;

    public CoordTest() {
        a=new Coord(0,1);
        b=new Coord(0,1);
        c=new Coord(4,5);
        d=new Coord(3,1);
        e=new Coord(0,2);
    }

    /**
     * Test of setCoord method, of class Coord.
     */
    @Test
    public void testSetCoord() {
        Coord tmp=new Coord(0,2);
        assertTrue("coord should be same as coord e",tmp.equals(e) );
        tmp.setCoord(0, 1);
        assertTrue("setting to same as coord a",tmp.equals(a) );
        
    }

    /**
     * Test of equals method, of class Coord.
     */
    @Test
    public void testEquals() {
        assertTrue("These coords should be the same",a.equals(b));
        assertTrue("if a==b then b==a",b.equals(a));
        assertTrue("a==a should be true",a.equals(a));
        assertFalse("These should not be the same",a.equals(c));
        assertFalse("if a!=b then b!=a",c.equals(a));
    }

    /**
     * Test of copy method, of class Coord.
     */
    @Test
    public void testCopy() {
        assertFalse("a and d are different",d.equals(a) );
        d=a.copy();
        assertTrue("d is a new copy",d.equals(a));
        
        
    }

    /**
     * Test of distanceFrom method, of class Coord.
     */
    @Test
    public void testDistanceFrom() {
        assertEquals(5,c.distanceFrom(e));
        assertEquals(0,b.distanceFrom(a));
        
    }

}