

package Model.Inventory.Items;


/**
 * Class implementing general Usable item 
 * @author Jan Svoboda-Fel-CVUT
 */
public class ItemUse extends Item {

    /**
     * Makes and item that is able to be used
     * @param path path in resources leading to the sprite
    */
    public ItemUse(String path) {
                super(path);
        usable=true;
    }

    /**
     * Method implementing how the item use should affect other classes
     */
    public void use() {
        System.out.print("Using Item:"+ItemUse.class.getName());
    }

}
