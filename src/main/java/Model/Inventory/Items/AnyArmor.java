

package Model.Inventory.Items;

/**
 * Class being a predecessor of any armour type
 * @author Jan Svoboda-Fel-CVUT
 */
public class AnyArmor extends ItemWear{
            
    protected int meeleReduction;
    protected int magicReduction;

    /**
     * Makes any armour and loads an item sprite based on path
     * @param path path in resources to item sprite
     */
    public AnyArmor(String path) {
        super(path);
    }
    
    /**
     * Gets you the magic reduction the armour provides
     * @return magic reduction in 0-100
     */
    public int getMagicReduction() {
        return magicReduction;
    }

    /**
     * Gets you the meele/physical reduction the armour provides
     * @return meele reduction in 0-100
     */
    public int getMeeleReduction() {
        return meeleReduction;
    }


    


}
