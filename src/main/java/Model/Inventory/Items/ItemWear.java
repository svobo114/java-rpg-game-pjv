package Model.Inventory.Items;

/**
 * Class implementing general Wearable item 
 * @author Jan Svoboda-Fel-CVUT
 */
public class ItemWear extends Item {

    /**
     * makes a wearable item based on a path to its sprite picture
     * @param path path in resources to sprite for this Item
     */
    public ItemWear(String path) {
        super(path);
        wearable=true;
    }

}
