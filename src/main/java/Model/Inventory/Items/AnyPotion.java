
package Model.Inventory.Items;

/**
 * Class implementing any general potion.
 * @author Jan Svoboda-Fel-CVUT
 */
public class AnyPotion extends ItemUse{
    protected int amount; // how much does the potion fill
    protected boolean toHp; //what does the potion fill? health or mana
    protected boolean toMana;

    /**
     * Makes a potion cass with path to the sprite
     * @param path path in resources leading to the sprite of the potion
     */
    public AnyPotion(String path) {
        super(path);
    }

    /**
     * Function specifying what happens when I drink the potion
     */
    protected void drink(){
    }        
    
    @Override
    public void use(){
        this.drink();
    }

    /**
     * Gets you how much does the potion fill
     * @return amount of points the potion fills
     */
    public int getAmount() {
        return amount;
    }

    /**
     * gets you boolean value saying if the potion fills HP
     * @return true if the potion fills health
     */
    public boolean isToHp() {
        return toHp;
    }

    /**
     * gets you the boolean vlaue saying if the potion fills mana
     * @return true if the potion fills mana
     */
    public boolean isToMana() {
        return toMana;
    }
    
    
    
}
