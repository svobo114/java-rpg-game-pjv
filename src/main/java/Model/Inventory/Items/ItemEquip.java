
package Model.Inventory.Items;

/**
 * Class implementing general Equipable item 
 * @author Jan Svoboda-Fel-CVUT
 */
public class ItemEquip extends Item {

    /**
     * Makes and item that is able to be equipped
     * @param path path in resources leading to the sprite
     */
    public ItemEquip(String path) {
        super(path);
        equipable=true;
    }

}
