
package Model.Inventory.Items;

/**
 * Class implementing a general scroll type item
 * @author Jan Svoboda-Fel-CVUT
 */
public class AnyScroll extends ItemUse{
    protected int damage;
    protected boolean toSelf;
    protected int distance;
    protected boolean reductable;

    /**
     * makes a scroll with a path to its sprite
     * @param path path in resources leading to the sprite for the item
     */
    public AnyScroll(String path) {
        super(path);
    }

    
    /**
     * function implementing effects of the scroll
     */
    protected void castSpell(){
    }    
    
    @Override
    public void use(){
        this.castSpell();
    }


    /**
     * Gets you the boolean value saying if the damage is to self
     * @return true if the damage is to self
     */
    public boolean isToSelf() {
        return toSelf;
    }

    /**
     * gets you the boolean saying if the damage is reductable (or "true")
     * @return true if the damage is able to be reduced
     */
    public boolean isReductable() {
        return reductable;
    }

    /**
     * Gets you the damage the spell does
     * @return amount of damage the spell gives
     */
    public int getDamage() {
        return damage;
    }

    /**
     * gets you the range of the spell
     * @return distance the spell affects
     */
    public int getDistance() {
        return distance;
    }
    
}
