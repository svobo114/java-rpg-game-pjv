package Model.Inventory.Items;

import java.util.concurrent.ThreadLocalRandom;

/**
 * A class implementing a general weapon
 * @author Jan Svoboda-Fel-CVUT
 */
public class AnyWeapon extends ItemEquip{
    protected int dmgLow;
    protected int dmgHigh;

    /**
     * makes a weapon based on a path to the sprite
     * @param path path in resources to the sprite to be loaded
     */
    public AnyWeapon(String path) {
        super(path);
    }
    
    
    /**
     * calculates a random damage in the bounds of min to max weapon damage
     * @return damage for the next swing of the weapon
     */
    public int attack(){
        //send event to make a sound
        int dmg=ThreadLocalRandom.current().nextInt(dmgLow, dmgHigh + 1);
        System.out.println("random damage from weapon: "+dmg);
        return dmg;
    }
     
    
}
