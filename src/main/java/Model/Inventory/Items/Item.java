

package Model.Inventory.Items;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

/**
 * A class implementing general Item that player can obtain, equip and use
 * Able to be saved to file - "Serializable"
 * @author Jan Svoboda-Fel-CVUT
 */
public class Item implements Serializable{
    protected String name="Item";
    protected int price=0;
    protected boolean wearable=false;
    protected boolean equipable=false;
    protected boolean usable=false;
    protected transient Background sprite; //Need to load item sprite
    protected String spriteName;
    protected static final Logger logger = Logger.getLogger(Item.class.getName());

    /**
     * Makes an item by loading the sprite from path
     * @param path path in resources that leads to the sprite for the item
     */
    public Item(String path) {
        changeSprite(path);
    }

    /**
     * gives you the boolean value meaning the item is equippable
     * @return true if the item is to be equipped
     */
    public boolean isEquipable() {
        return equipable;
    }

    /**
     * Returns you the boolean value meaning if the item is to be used
     * @return true if the item is usable
     */
    public boolean isUsable() {
        return usable;
    }

    /**
     * Returns you the boolean value meaning if the item is to be weared
     * @return true if the item is wearable
     */
    public boolean isWearable() {
        return wearable;
    }

    /**
     * gets you the name of the item 
     * @return string meaning name
     */
    public String getName() {
        return name;
    }

    /**
     * gets you the sprite of the item (in a form of Background)
     * @return Background that can be used as a sprite
     */
    public Background getSprite() {
        return sprite;
    }
    
    /**
     * Reload the sprite based on the path
     * @param path path in reosurces leading to the item sprite
     */
    public void changeSprite(String path){
        this.spriteName = path;
        
        Image picture=null;
        try{
            picture = new Image(path);
            logger.log(Level.FINE,"image for item sprite loaded");
        }catch (Exception e){
            logger.log(Level.SEVERE, "Unable to load a background for an item sprite, Shutting down: " + e.toString());
            System.exit(-1);
        }
        BackgroundImage tmp= new BackgroundImage(picture, BackgroundRepeat.SPACE, BackgroundRepeat.SPACE, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        sprite = new Background(tmp);
    }
    
    /**
     * Function that reloads all necessary parts of the item that do not save 
     */
    public void reload(){
        changeSprite(spriteName);
    }
}
