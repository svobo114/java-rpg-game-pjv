
package Model.Inventory.Items.Individual;

import Model.Inventory.Items.AnyPotion;

/**
 * Class for Elixir of mana available in the game
 * The elixir would add mana to player
 * @author Jan Svoboda-Fel-CVUT
 */
public class ElixirOfMana extends AnyPotion{

    /**
     * Makes a new instance of elixir of mana adding 15 mana
     */
    public ElixirOfMana() {
        super("objects/errorN.png");
        price=30;
        name="The Elixir of Mana";
        amount=15;
        toHp=false;
        toMana=true;
    }
    
    @Override
    protected void drink() {
        //calls an event to add mana to player
    }
    
}
