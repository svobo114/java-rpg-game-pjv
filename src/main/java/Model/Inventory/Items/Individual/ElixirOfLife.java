package Model.Inventory.Items.Individual;

import Model.Inventory.Items.AnyPotion;

/**
 * Class for Elixir of life available in the game
 * The elixir would add health to player/ heal him
 * @author Jan Svoboda-Fel-CVUT
 */
public class ElixirOfLife extends AnyPotion{
    
    /**
     * Makes a new Elixir of life
     */
    public ElixirOfLife() {
        super("objects/errorN.png");
        price=30;
        name="The Elixir of life";
        amount=35;
        toHp=true;
        toHp=false;
        
    }
    
    @Override
    protected void drink() {
        //calls an event to add health to player
    }
    
    
}
