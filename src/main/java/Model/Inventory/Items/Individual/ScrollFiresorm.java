

package Model.Inventory.Items.Individual;

import Model.Inventory.Items.AnyScroll;

/**
 * Class implementing a Scroll for firestorm usable by the player
 * @author Jan Svoboda-Fel-CVUT
 */
public class ScrollFiresorm extends AnyScroll{

    /**
     * Makes an instance of a firescroll
     */
    public ScrollFiresorm() {
        super("objects/errorN.png");
        price=30;
        name="Scroll of Pocket Firestorm";    
        damage=40;
        distance=0;
        reductable=true;
        toSelf=false;
    }

    @Override
    protected void castSpell() {
        //apply damage in range?
    }
}
