
package Model.Inventory.Items.Individual;

import Model.Inventory.Items.AnyWeapon;

/**
 * Class implementing a sword equipable by the player
 * @author Jan Svoboda-Fel-CVUT
 */
public class MeeleSword extends AnyWeapon{

    
    /**
     * Makes you an instance of a sword, which is a reliably strong weapon in the game
     */
    public MeeleSword() {
        super("objects/swordN.png");
        price=120;
        name="Steel Sword";
        dmgHigh=40; 
        dmgLow=10;
    }
}
