

package Model.Inventory.Items.Individual;

import Model.Inventory.Items.AnyScroll;

/**
 * Class implementing a Scroll usable by the player
 * Healing scroll heals the player
 * @author Jan Svoboda-Fel-CVUT
 */
public class ScrollHealing extends AnyScroll{

    /**
     * makes an instance of a healing scroll
     */
    public ScrollHealing() {
        super("objects/errorN.png");
        price = 50;
        damage=-50;
        distance=0;
        reductable=false;
        toSelf=true;
        name="Scroll of Healing";
    }

    @Override
    protected void castSpell() {
        //apply damage, but this time its negative (so heal)
    }
    
    
    

}
