

package Model.Inventory.Items.Individual;

import Model.Inventory.Items.AnyArmor;

/**
 * Class for 1st Armour available in the game
 * @author Jan Svoboda-Fel-CVUT
 */
public class Armour1 extends AnyArmor{

    /**
     * Makes a new armour1 
     */
    public Armour1() {
        super("objects/armor1N.png");
        price=100;
        name="Leather Armor";
        magicReduction=10;
        meeleReduction=20;
    }

}
