

package Model.Inventory.Items.Individual;

import Model.Inventory.Items.AnyWeapon;

/**
 * Class for best sword available in the game
 * @author Jan Svoboda-Fel-CVUT
 */
public class BestSword extends AnyWeapon{

    /**
     * Makes a new best sword item
     */
    public BestSword() {
        super("objects/sword2N.png");
         price=120;
        name="Best Sword";
        dmgHigh=45; 
        dmgLow=25;
    }

}
