

package Model.Inventory.Items.Individual;

import Model.Inventory.Items.AnyArmor;

/**
 * This is a joke potion class which was meant to be wearable instead of usable
 * This class ended up not being used anywhere in the game
 * @author Jan Svoboda-Fel-CVUT
 */
public class ElixirStardust extends AnyArmor {
        private final int meeleReduction;
        private final int magicReduction;
    
        /**
         * Makes a new instance of Stardust elixir
         */
     public ElixirStardust() {
        super("objects/errorN.png");
        price =350;
        name="The Stardust Elixir";
        
        //has to have values for armour, is wearable
        usable=false;
        wearable=true;
        meeleReduction=90;
        magicReduction=75;
    }

     //this class Does not have any sprite yet, isnt used in the game, but theoretically could
    
}
