

package Model.Inventory.Items.Individual;

import Model.Inventory.Items.AnyArmor;

/**
 *  * Class for 2nd Armour available in the game
 * @author Jan Svoboda-Fel-CVUT
 */
public class Armour2 extends AnyArmor{

    /**
     * Makes a new armour 2
     */
    public Armour2() {
        super("objects/armor2N.png");
        price=250;
        name="Dragon Scale Armor";
        magicReduction=30;
        meeleReduction=50;
    }

}
