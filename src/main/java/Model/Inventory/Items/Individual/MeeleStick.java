

package Model.Inventory.Items.Individual;

import Model.Inventory.Items.AnyWeapon;

/**
 * Class implementing a stick equipable by the player
 * @author Jan Svoboda-Fel-CVUT
 */
public class MeeleStick extends AnyWeapon{

    /**
     * Makes you an instance of a stick, which is the weakes weapon in the game
     */
    public MeeleStick() {
        super("objects/stickN.png");
        price=10;
        name=" Wooden Stick";
        dmgHigh=10;
        dmgLow=3;             
    }
}
