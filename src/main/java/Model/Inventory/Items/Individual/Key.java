

package Model.Inventory.Items.Individual;

import Model.Inventory.Items.Item;

/**
 * Class implementing a key that is used to open a chest with the same key code
 * @author Jan Svoboda-Fel-CVUT
 */
public class Key extends Item{
    private final String key;
    
    /**
     * Makes a key with secret chest code 
     * @param chestCode is set to be the key code for opennign chests
     */
    public Key(String chestCode) {
        super("objects/keyN.png");
        key=chestCode;
        name="key: "+ chestCode;
    }
    
    /**
     * gets you the keyCode from the key 
     * to see if you have the key you need for a particular chest to open
     * @return String meaning the keycode
     */
    public String getKey(){
        return key;
    }

}
