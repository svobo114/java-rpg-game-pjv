package Model.Inventory.Items.Individual;

import Model.Inventory.Items.AnyWeapon;

/**
 * Class implementing a dagger equipable by the player
 * @author Jan Svoboda-Fel-CVUT
 */
public class MeeleDagger extends AnyWeapon{

    /**
     * makes you an instance of a dagger, which is the second weakest meele item in the game
     */
    public MeeleDagger() {
        super("objects/daggerN.png");
        price=60;
        name="Iron dagger";
        dmgHigh=15;
        dmgLow=7;
    }
    
}
