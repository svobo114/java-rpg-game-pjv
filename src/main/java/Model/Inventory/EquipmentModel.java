

package Model.Inventory;

import Model.FileSaver;
import Model.Inventory.Items.AnyArmor;
import Model.Inventory.Items.AnyWeapon;
import Model.Inventory.Items.Individual.Armour1;
import Model.Inventory.Items.Individual.MeeleStick;
import Model.Inventory.Items.Item;
import Model.Inventory.Items.ItemUse;
import java.io.Serializable;
import java.util.logging.Logger;

/**
 *  Holds Items that a player uses
 * @author Jan Svoboda-Fel-CVUT
 */
public class EquipmentModel implements Serializable{
    private static final Logger logger = Logger.getLogger(EquipmentModel.class.getName());
    /** default armour that the player has equipped, when he does not have anything else equipped */
    public AnyArmor defaultArmor =  new Armour1(); 
    /**default weapon that the player has equipped in the beginning or when he does not have anything else */
    public AnyWeapon defaultWeapon = new MeeleStick(); 
    /** players current armour */
    public AnyArmor myArmor; 
     /** players current weapon */
    public AnyWeapon myWeapon;
    private ItemUse quick1;
    

    /**
     * Basic constructor, makes default weapon and armor invisible in the equipment panel
     */
    public EquipmentModel() {
        this.myArmor=defaultArmor;
        this.myWeapon= defaultWeapon;
        this.quick1=null;
        makeDefaultsInvisible();
    }
    
    /**
     * Changes the bg of default equipment to not be vidible in the menu
     */
    public void makeDefaultsInvisible(){
        defaultArmor.changeSprite("ui/armorSlot.png");
        defaultWeapon.changeSprite("ui/weaponSlot.png");
    }
    
    /**
     * calculate attack damage based on the weapon properties
     * Invokes the weapons damage function, which takes a random number from-to
     * @return damage value given
     */
    public int calculateAttack(){
        if (myWeapon==null){
            return 1;
        }else{
            return myWeapon.attack();
        }
    }
    
    /**
     * Calculate meele defense based on the gear properties
     * @param dmgTaken meele damage recieved
     * @return damage player should suffer
     */
    public int calculateMeeleDefense(int dmgTaken){
        if (myArmor==null){//which should not happen
            return dmgTaken;
        }else{
            return (dmgTaken*(100-myArmor.getMeeleReduction()))/100;
        }
    }
    
    /**
     * Calculates defense agains magic damage based on the equipped gear
     * @param dmgTaken Max amount of damage taken
     * @return the damage taken after mitigating it with armor
     */
    public int calculateMagicDefense(int dmgTaken){
        if (myArmor==null){
            return dmgTaken;
        }else{
            return (dmgTaken*(100-myArmor.getMagicReduction()))/100;
        }
    }
    
    /**
     * Use an Item equipped in the quick slot
     */
    public void useQuickslot(){
        //possible enhancement/implementation
        //check equipped item
        //use it
        //if amount<=0 -> delete it from the system and notify graphics via event to redraw
    }
    
    /**
     * equip item into slot that it belongs to
     * @param e item to be equiped
     */
    public void equip(Item e){
        if (e.isEquipable()){
            equipWeapon((AnyWeapon) e);
        }
        else if (e.isUsable()){
            equipQuicslot();
        }
        else if (e.isWearable()){
            equipArmor((AnyArmor) e);
        }else{
            
        }
        
    }
    
    /**
     * de equip an item from slot it is in 
     * @param e  Item to be deequiped
     */
    public void deEquip(Item e){
        if (e.isEquipable()){
            myWeapon=defaultWeapon;
        }
        else if (e.isUsable()){
            quick1=null;
        }
        else if (e.isWearable()){
            myArmor=defaultArmor;
        }
    }
    

    private void equipArmor(AnyArmor newArm){
        myArmor=newArm;
    }
    
    private void equipWeapon(AnyWeapon newWeap){
        myWeapon=newWeap;
    }

    private void equipQuicslot(){
        
    }
    
    
}
