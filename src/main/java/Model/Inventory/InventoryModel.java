

package Model.Inventory;

import Model.FileSaver;
import Model.Inventory.Items.Individual.Armour2;
import Model.Inventory.Items.Individual.ElixirStardust;
import Model.Inventory.Items.Individual.Key;
import Model.Inventory.Items.Individual.MeeleStick;
import Model.Inventory.Items.Individual.MeeleSword;
import Model.Inventory.Items.Item;
import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;

/**
 *  Holds all player items currently not in use
 * @author Jan Svoboda-Fel-CVUT
 */
public class InventoryModel implements Serializable{
    private static final Logger logger = Logger.getLogger(InventoryModel.class.getName());
    private static final int  maxSize=16;
    private List<Item> inventoryList=new CopyOnWriteArrayList<>();
    

    /**
     * Basic constructor, adds some basic items in the beginning (may be overwritten by an inventory when loading
     */
    public InventoryModel() {
        addItem(new MeeleSword());
        addItem(new Armour2());
        addItem(new MeeleStick());
        addItem(new Key("chest1"));
        //addItem(new ElixirStardust()); //will load with error as a sprite - to see if error works. is still wearable
    }
    
    /**
     * Add an item to inventory
     * @param e item to be added
     */
    public void addItem(Item e){
        if(inventoryList.size()< maxSize){
          inventoryList.add(e);  
        }
    }
    
    /**
     * gets you the current size of inventory list (NOT maximum size)
     * @return integer for size of the lsit holding items in inventory
     */
    public int getSize(){
        return inventoryList.size();
    }
    
    /**
     * gets you the list of items that represents the current players inventory
     * @return List of items
     */
    public List<Item> getInventory(){
        return inventoryList;
    }

    /**
     * remove an item from the inventory
     * @param I item to be removed
     */
    public void removeItem(Item I){
        inventoryList.remove(I);
    }
}
