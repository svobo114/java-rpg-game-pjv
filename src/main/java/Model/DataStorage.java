

package Model;

import Controller.MainController;
import Misc.Coord;
import Misc.Hitbox;
import Model.Entities.Entity;
import Model.Entities.Individual.Player;
import Model.Inventory.EquipmentModel;
import Model.Inventory.InventoryModel;
import Model.Inventory.Items.Item;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Data storage class- holds data that can be then loaded back into main game classes to load a saved game
 * @author Jan Svoboda-Fel-CVUT
 */
public class DataStorage implements Serializable{
    private static final Logger logger = Logger.getLogger(DataStorage.class.getName());
//player init 
    private InventoryModel playersInv;
    private EquipmentModel playersEq;
    private int playerMana;
    private int playerHealth;
    private int playerGold;

//controler init
    private List<Entity> listEntity; 
    private Coord windowCoord;
    private Coord playerScreenCoord; 
    private Hitbox playerHitbox;
        

    /**
     * default constructor, has to be filled with "storeController"
     */
    public DataStorage() {
    }
    
    private void storePlayer(Player p){
        playersEq=p.getEquipmentModel();
        playersInv=p.getInventoryModel();
        playerMana=p.getCurrentMana();
        playerGold=p.getCurrentMoney();
        playerHealth=p.getCurrentHealth();
        logger.log(Level.FINE, "Player info stored");
    }
    
    /**
     * Store controller and its member classes to storage for writing to files
     * @param c The controller being written down
     */
    public void storeController(MainController c){
        storePlayer(c.getPlayerModel());
        listEntity=c.getEntities();
        windowCoord=c.getWindowCoord();
        playerScreenCoord=c.getPlayerScreenCoord();
        playerHitbox=c.getPlayerHB();
        logger.log(Level.FINE, "Controller info stored");
    }
        
    /**
     * reloads sprites of items AND entities ofter loading the classes from a file
     * @param c a MainController is needed in entities to connect to when reloading
     */
    public void reloadSprites(MainController c){
        logger.log(Level.FINE, "Reloading Sprites");
        for (Entity entity : listEntity) {
            //reload
            entity.reload(c);
        }
        for (Item item : playersInv.getInventory()) {
            //reload
            item.reload();
            
        }
        
        playersEq.myArmor.reload();
        playersEq.myWeapon.reload();
        playersEq.makeDefaultsInvisible();
        
    }

    /**
     * Gets you the players inventory model, that hodls players items
     * @return the players inventory that was put into storage
     */
    public InventoryModel getPlayersInv() {
        return playersInv;
    }

    /**
     * gets you the players equipment model that calculates given and recieved damage in Player
     * @return the inventory that was put into storage
     */
    public EquipmentModel getPlayersEq() {
        return playersEq;
    }

    /**
     * Gets you players current mana at the time of saving
     * @return the players mana that was stored
     */
    public int getPlayerMana() {
        return playerMana;
    }

    /**
     * Gets you players current health at the time of saving
     * @return the players health that was stored
     */
    public int getPlayerHealth() {
        return playerHealth;
    }
    
    /**
     * Gets you players current gold/money at the time of saving
     * @return the players gold that was stored
     */
    public int getPlayerGold() {
        return playerGold;
    }

    /**
     * Gets you the entity list from controller, that was put in. the entities need to reload after
     * @return the copyOnWrite list of entities on the map
     */
    public List<Entity> getListEntity() {
        return listEntity;
    }

    /**
     * Gets you the coordinates of window inside the map, from controller
     * @return the Coord of window
     */
    public Coord getWindowCoord() {
        return windowCoord;
    }

    /**
     * Gets you the coordintes of the player on the screen , from controller
     * @return the Coord of player on window
     */
    public Coord getPlayerScreenCoord() {
        return playerScreenCoord;
    }

    /**
     * Gets you the hitbox of player that is used for collisions in controller
     * @return the Hitbox of player from controller
     */
    public Hitbox getPlayerHitbox() {
        return playerHitbox;
    }

    
}
