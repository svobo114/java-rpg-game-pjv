

package Model.Entities;

import Controller.MainController;
import Misc.Hitbox;
import Misc.Velocity;
import java.io.Serializable;
import java.util.logging.Logger;
import javafx.scene.image.Image;

/**
 * generic class for making entities on the map
 * @author Jan Svoboda-Fel-CVUT
 */
public class Entity implements Serializable{
    /** map constant - width */
    protected final static int _MAP_W =3310; 
    /** map constant - height*/
    protected final static int _MAP_H =1748; 
    protected static final Logger logger = Logger.getLogger(Entity.class.getName());
    /** hitbox of the entity*/
    protected Hitbox hitbox;
    /**Velocty of the entity = where is it moving*/
    protected Velocity currentVelocity;
    /**path tot he resources for animation or background image*/
    protected String pathToReload="";
    

    /**
     * Basic constructor, assigns hitbox to entity, and sets it to not move
     * @param hitbox Hitbox the entity will ocupy
     */
    public Entity(Hitbox hitbox) {
        this.hitbox = hitbox;
        currentVelocity=new Velocity(0,0); //entities dont move, unless the are mobs
        
    }

    
    /**
     * Returns the frame that currntly represents the entity. either its picture or an animation frame
     * @param currentMStime timestamp of current time (for extrapolating the frame)
     * @return Image frame that currently represents the entity
     */
    public Image whatFrame(double currentMStime){
        //to see, what frame of animation I should be on, or just the pic.
        return null;
    }

    /**
     * Gets you the health percentage of entity from max health
     * @return health percentage of entity out of max health
     */
    public int getHealthPercentage(){
        return 0;
    }
      
    /**
     * Gets you the COPY of current hitbox
     * @return a copy of entity hitbox
     */
    public Hitbox getHitbox() {
    return hitbox.returnCopy();
    }
    
    /**
     * Sets movement to new velocity
     * @param currentMovement new velocity for the entity
     */
    public void setCurrentMovement(Velocity currentMovement) {
        //do nothing, movement is still 0,0
    }
    
    /**
     * gets yout the current velocity from entity
     * @return velocity of the entity
     */
    public Velocity getCurrentMovement(){
        return new Velocity(0,0);
    }
    
    /**
     * Tells the entity to move in a current velocity
     */
    public void move(){
    }
    
    /**
     * Tells entity to update base don recieved damage
     * @param meeleDmg meele/physical component of damage
     * @param magDmg magical component of damage
     */
    public void getAttacked(int meeleDmg,int magDmg){
        /*
        nothing here
        when attacking into trees and walls... will be updated in child classes to reflect them
        */
    }
    
    /**
     * reloads the entity when its being loaded from storage
     * to reload the animation or picture and connect the entity to controller if it needs it
     * @param c controller used to connect the entity.
     */
    public void reload(MainController c){}
     
    

}
