

package Model.Entities.MobAI;

import Model.Entities.Mob;
import java.util.logging.Logger;
import javafx.animation.AnimationTimer;

/**
 *  Simple AI for enemies. 
 *  Will stand on the spot, sometimes move somewhere around
 * 
 * @author Jan Svoboda-Fel-CVUT
 */
public class SimpleMobAI extends Thread {
    private static final Logger logger = Logger.getLogger(SimpleMobAI.class.getName());
    private long startNanoTime=0;
    private double [] lengths= {0.5,1.0,1.0,2.0};
    private double currentLenght=1.0;
    private double timeOfLast;
    private Mob parent;
    
    /**
     * Basic constructor connecting the AI thread to the mob and running it
     * @param parent parent mob -> AI is connected to it and chooses a new velocity after a random time
     */
    public SimpleMobAI(Mob parent) {
        this.parent=parent;
        timeOfLast=0;
        this.start();
    }
    
    
    
    @Override
    public void run(){
        startNanoTime = System.nanoTime();
        new AnimationTimer(){
            @Override
            public void handle(long currentNanoTime){
                double time = (currentNanoTime - startNanoTime) / 1000000000.0; 
                if (time-timeOfLast>currentLenght){ //time to switch movement
                    timeOfLast=time;
                    parent.newRandomVelocity();
                }
            }   
        }.start();
    }

    /**
     * Set up different lengths for the random movements
     * @param lengths An array of lengths (in seconds) - different lengths will result in mode differing behaviour
     */
    public void setLengths(double [] lengths) {
        this.lengths = lengths;
        this.currentLenght=lengths[0];
    }
    
    
    
}
