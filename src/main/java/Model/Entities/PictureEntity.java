

package Model.Entities;

import Controller.MainController;
import Misc.Coord;
import Misc.Hitbox;
import static Model.Entities.Entity.logger;
import java.util.logging.Level;
import javafx.scene.image.Image;

/**
 *
 * @author Jan Svoboda-Fel-CVUT
 */
public class PictureEntity extends Entity{
    protected transient Image picture=null; //used for not animated entities

    /**
     * Makes a new picture entity based on hitbox parameters
     * @param x hitbox left upper point x coordinate
     * @param y hitbox left upper point y coordinate
     * @param width hitbox width
     * @param height hitbox height
     */
    public PictureEntity(int x, int y,int width, int height) {
        super(new Hitbox(new Coord(x,y),_MAP_W,_MAP_H,width,height));
        
    }
    
    /**
     * load image from path and sets it to be the entity image
     * @param path path to image in resource folder
     */
    public void setImage(String path){
        pathToReload=path;
        try{
            picture = new Image(path);
            logger.log(Level.FINE,"image for picture entity loaded");
        }catch (Exception e){
            logger.log(Level.SEVERE, "Unable to load some Resource for a PictureEntity, Shutting down: " + e.toString());
            System.exit(-1);
        }
    }
     
    
    @Override
    public Image whatFrame(double currentMStime){
        return picture;
    }

    @Override
    public void reload(MainController c) {
       setImage(pathToReload);
    }
    
    
    

}
