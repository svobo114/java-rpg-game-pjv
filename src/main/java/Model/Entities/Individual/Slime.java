

package Model.Entities.Individual;

import Controller.MainController;
import Model.Entities.Mob;
import Model.Entities.MobAI.SimpleMobAI;
import View.AnimationModel;

/**
 * Slime class extending a mob. will spawn an entity of slime moving on the map
 * @author Jan Svoboda-Fel-CVUT
 */
public class Slime extends Mob{
    private static final int _SLIME_W=64;
    private static final int _SLIME_H=64;
    

    /**
     * Slime constructor that needs hitbox coordinates and a controller to connect to
     * @param x x coord of left upper point of hitbox
     * @param y y coord of ledt upper point of hitbox
     * @param c Controller that the entity connects to
     */
    public Slime(int x, int y,MainController c) {//make a beast with coords
        super(x, y, _SLIME_W, _SLIME_H,c);
        pathToReload="monsters/slime";
        reward=90;
        magicReduction=0;
        meeleReduction=20;
        nimDmg=2;
        maxDmg=7;
        maxHealth=50;
        currentHealth=maxHealth;
        setAnimationModel(new AnimationModel(pathToReload));
        aiThread=new SimpleMobAI(this);
    }
    
    

}
