

package Model.Entities.Individual;

import Controller.MainController;
import Model.Entities.PictureEntity;
import Model.Inventory.Items.Item;
import java.util.List;
import java.util.logging.Level;

/**
 * Chest class entity - holds inventory, is openable by a key
 * @author Jan Svoboda-Fel-CVUT
 */
public class Chest extends PictureEntity{
    private static final int _CHEST_W=64;
    private static final int _CHEST_H=64;
    private String secretKeyText;
    private List<Item> inventory;

    /**
     * makes a chest based on position, secret key text and inventory inside
     * @param x x coordinate of the chest left upper corner
     * @param y y coordinate of the chest left upper corner
     * @param key secret key text that will be matched with a key
     * @param inv inventory the chest has
     */
    public Chest(int x, int y, String key, List<Item> inv) {
        super(x, y, _CHEST_W, _CHEST_H);
        setImage("map/chest.png");
        logger.log(Level.FINE, "Chest made");
        secretKeyText=key;
        inventory=inv;
    }

    /**
     * gets you the inventory hidden inside the chest
     * @return List of items representing the inventory
     */
    public synchronized List<Item> getInventory() {
        return inventory;
    }

    /**
     * add an item to the inventory of the chest
     * @param e Item to be added
     */
    public synchronized void addItem(Item e){
        inventory.add(e);
    }
    
    /**
     * Remove an item from the inventory of the chest
     * @param e itme to be removed 
     */
    public synchronized void removeItem(Item e){
        inventory.remove(e);
    }

    /**
     * Function checking the key to be the same as the chest secret key
     * @param key key to be checked against the chess's secret key
     * @return true if the keys match
     */
    public boolean checkKey(String key){
        return key.equals(secretKeyText);
    }

    @Override
    public void reload(MainController c) {
        setImage(pathToReload);
        for (Item item : inventory) {
            item.reload();
        }
    }
    
    
}
