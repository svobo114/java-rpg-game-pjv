

package Model.Entities.Individual;

import Model.Inventory.EquipmentModel;
import Model.Inventory.InventoryModel;
import Model.Inventory.Items.Item;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class holding player information and current inventory+equipment
 * @author Jan Svoboda-Fel-CVUT
 */
public class Player implements Serializable{
    private static final Logger logger = Logger.getLogger(Player.class.getName());
    private EquipmentModel equipment;
    private InventoryModel inventory;
     //constant stats.
    private final static int maxMana=50;
    private final static int maxHealth=100;
        
    //changing stats
    private int currentHealth;
    private int currentMana;
    private int currentMoney;
    
    /**
     * makes a new player instance with default values
     */
    public Player() {
        currentMoney=80;
        currentHealth=maxHealth;
        currentMana=maxMana;
        equipment=new EquipmentModel();
        inventory=new InventoryModel();
    }
    
    /**
     * gets you the players inventory as a list of items
     * @return List of items which is  the players inventory
     */
    public List<Item> getInventory(){//returns current characters inventory
        return inventory.getInventory();
    }
    
    /**
     * Gets you the players equipment model
     * @return model of equipment with weapon and armour currently equipped
     */
    public EquipmentModel getEquipmentModel(){
        return equipment;
    }
    
    /**
     * Gets you the whole inventory model of a player
     * @return inventory model holding players items
     */
    public InventoryModel getInventoryModel(){
        return inventory;
    }

    /**
     * gets you players current health
     * @return integer meaning current health
     */
    public int getCurrentHealth(){
        return currentHealth;
    }

    /**
     * gets you the players maximum possible health
     * @return integer meaning MAX health
     */
    public int getMaxHealth() {
        return maxHealth;
    }

    /**
     * gets you the players maximum possible mana
     * @return integer meaning MAX mana
     */
    public int getMaxMana() {
        return maxMana;
    }

    /**
     * gets you the players current mana
     * @return integer meaning current mana
     */
    public int getCurrentMana() {
        return currentMana;
    }

    /**
     * gets you players current gold/amount of money
     * @return amount of players money
     */
    public int getCurrentMoney() {
        return currentMoney;
    }
    
    
     /**
      * Gets you the amount of damage player would have done if he hit
      * @return the amount of damage calculated by the equipment 
      */       
    public int swingWeapon(){
        return equipment.calculateAttack();
        //possible improvement: sets the event for sound
    }
    
    /**
     * calculates the damage to self based on meele and magic damage
     * @param meeleDmg Meele part of the damage
     * @param magDmg magical part of the damage
     */
    public void takeDamage(int meeleDmg,int magDmg){
        currentHealth-=meeleDmg;
        currentHealth-=magDmg;
        if (currentHealth<=0){
            //died, do something
        }
        //possible: set the sound event as being hit
    }

    /**
     * adds the amount of gold to player
     * @param amount amount of gold added to player
     */
    public synchronized void addMoney(int amount) {
        logger.log(Level.INFO, "{0}Money added", Integer.toString(amount));
        currentMoney+=amount;
    }

    /**
     * reads if the player has more money than amount
     * @param amount amount of money player has to have
     * @return true if player has MORE (or Equal) money than the amount
     */
    public boolean hasEnoughMoney(int amount) {
        return currentMoney>=amount;
    }
    
    
    /**
     * equip an item from players inventory and put it in the equipment (only if able)
     * @param I item ot be equipped
     */
    public void equipItem(Item I){
         if (I.isEquipable()){
            deEquipItem(equipment.myWeapon);
        }
        else if (I.isUsable()){
            //equip usables
        }
        else if (I.isWearable()){
            deEquipItem(equipment.myArmor);
        }else{
            return;
        }
        equipment.equip(I);
        inventory.removeItem(I);
    }

    /**
     * de equip item from players equipment model and put it in the inventory
     * @param I Item to be deequipped
     */
    public void deEquipItem(Item I){
        if( I != equipment.defaultWeapon && I != equipment.defaultArmor){
            inventory.addItem(I);
        }
        equipment.deEquip(I);
        
    }
    
    /**
     * add item to players inventory
     * @param I item to be added
     */
    public void additem(Item I){
        inventory.addItem(I);
    }

    /**
     * Fills the player model from storage class
     * @param cHe current Health
     * @param cMa current mana
     * @param cMo current money/gold
     * @param inv players inventory model
     * @param eq plaxers equipment model
     */
    public void fillFromStorage(int cHe,int cMa, int cMo,InventoryModel inv, EquipmentModel eq){
        this.currentHealth=cHe;
        this.currentMana=cMa;
        this.currentMoney=cMo;
        this.equipment=eq;
        this.inventory=inv;
    }

}
