

package Model.Entities.Individual;

import Model.Entities.PictureEntity;
import java.util.logging.Level;


/**
 * Wall class representing map entity that is a wall (and has no animation)
 * @author Jan Svoboda-Fel-CVUT
 */
public class Wall extends PictureEntity{
    
    /**
     *  constructor that needs hitbox coordinates and width + height
     * @param x x coord of left upper point of hitbox
     * @param y y coord of left upper point of hitbox
     * @param width width of the wall. can be up to 700
     * @param height height of the wall. can be up to 700
     */
    public Wall(int x, int y,int width, int height ) {
        super(x, y, width, height);
        setImage("map/wall700.png");
        logger.log(Level.FINE, "Wall made");
    }

    
}
