

package Model.Entities.Individual;

import Controller.MainController;
import Model.Entities.Mob;
import View.AnimationModel;

/**
 * Skeleton class extending a mob. will spawn an skeleton entity moving on the map
 * @author Jan Svoboda-Fel-CVUT
 */
public class Skeleton extends Mob{
    private static final int _SKELETON_W=128;
    private static final int _SKELETON_H=128;
   
    /**
     * Constructor that needs hitbox coordinates and a controller to connect to
     * @param x x coord of left upper point of hitbox
     * @param y y coord of ledt upper point of hitbox
     * @param c Controller that the entity connects to
     */
    public Skeleton(int x, int y,MainController c) {
        super(x,y,_SKELETON_W,_SKELETON_H,c);
        pathToReload="monsters/skeleton";
        reward=150;
        magicReduction=25;
        meeleReduction=15;
        nimDmg=5;
        maxDmg=12;
        maxHealth=70;
        currentHealth=maxHealth;
        animationModel=new AnimationModel(pathToReload);
    }

}
