

package Model.Entities.Individual;

import Model.Entities.PictureEntity;
import java.util.logging.Level;

/**
 * tree entity class. will spawn a tree on the map
 * @author Jan Svoboda-Fel-CVUT
 */
public class Tree extends PictureEntity{
    private static final int _TREE_W=89;
    private static final int _TREE_H=152;

    /**
     * Tree constructor that needs hitbox coordinates
     * @param x x coord of left upper point of hitbox
     * @param y y coord of ledt upper point of hitbox

     */
    public Tree(int x, int y ) {
        super(x, y, _TREE_W, _TREE_H);
        setImage("map/tree.png");
        logger.log(Level.FINE, "Tree made");
    }

    

    
}
