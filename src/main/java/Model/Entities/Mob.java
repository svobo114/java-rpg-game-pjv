
package Model.Entities;

import Controller.MainController;
import Misc.Coord;
import Misc.Hitbox;
import Misc.Velocity;
import Model.Entities.MobAI.SimpleMobAI;
import View.AnimationModel;
import java.util.Random;
import java.util.logging.Level;

/**
 *  Class implementing mob entity which has animation, AI, moves and is able to be killed
 * @author Jan Svoboda-Fel-CVUT
 */
public class Mob extends AnimEntity{
    /** reward for killing the mob */
    protected int reward;
    protected transient MainController c;
    protected transient SimpleMobAI aiThread; //tells how to behave
    private  static Velocity [] velocities = {new Velocity(1,1),new Velocity(-1,-1),new Velocity(1,-1),new Velocity(-1,1),new Velocity(0,0)}; //a set of velocities
    
    //for calculating if alive
    protected int maxHealth;
    protected int currentHealth;
    
    //for calculating incoming dmg
    protected int meeleReduction;
    protected int magicReduction;
    
    //for calculating outcome dmg
    protected int nimDmg;
    protected int maxDmg;
    
    protected int hitW;
    protected int hitH;
    
    
    /**
     * Makes a new Mob based on hitbox and controller
     * @param x hitbox left upper point x coordinate
     * @param y hitbox left upper point y coordinate
     * @param w hitbox width
     * @param h hitbox height
     * @param c controller that the mob connects to
     */
    public Mob(int x, int y,int w, int h,MainController c){
        super(new Hitbox(new Coord(x,y),_MAP_W,_MAP_H, w, h));
        maxHealth=-1;
        this.c=c;
        currentHealth=maxHealth;
        logger.log(Level.FINE,"New Mob initialised");
        
        aiThread=new SimpleMobAI(this);
    }
    
    @Override
    public void getAttacked(int meeleDmg,int magDmg){
        int dmg=(meeleDmg*(100-meeleReduction)+magDmg*(100-magicReduction))/100;
        currentHealth-=dmg;
        if (currentHealth<=0){
            c.giveMoney(reward);
            die();
        }
    }
    
    /**
     * what to do when the mob attacks somewhere
     */
    private void attack(){
        
    }
    
    /**
     * Function that reacts to the mob dying.
     * Future continuation: play a dying sound, spawn a corpse and so on
     */
    private void die(){
        c.killEnt(this);
    }
    
    /**
     * Set velocity based on X and Y coordinates
     * @param x x component of new velocity
     * @param y y component of new velocity
     */
    public void changeVelocity(int x, int y){
        currentVelocity=new Velocity (x,y);
    }
    
    @Override
    public synchronized int getHealthPercentage(){
        return ((hitbox.width * currentHealth)/maxHealth);
    }
    
    @Override
    public void setCurrentMovement(Velocity currentMovement) {
    this.currentVelocity = currentMovement;
    }
    
    @Override
    public synchronized Velocity getCurrentMovement(){
        return currentVelocity;
    }
    
    /**
     * when prompted, the entity finds a new random velocity
     */
    public synchronized void newRandomVelocity(){
        int rnd = new Random().nextInt(velocities.length);
         setCurrentMovement(velocities[rnd]);
    }
    
    @Override
    public void move(){
        hitbox.move(currentVelocity);
    }  

    @Override
    public void reload(MainController c) {
        setAnimationModel(new AnimationModel(pathToReload));
        aiThread=new SimpleMobAI(this);
        this.c =c;
    }

    /**
     * Sets the main controller 
     * @param c the MainController to be set in Mob
     */
    public void setC(MainController c) {
        this.c = c;
    }
    
    
    
    
}
