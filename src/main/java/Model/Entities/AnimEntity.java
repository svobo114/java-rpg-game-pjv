

package Model.Entities;

import Misc.Hitbox;
import View.AnimationModel;
import javafx.scene.image.Image;

/**
 * Animated entity. Has an animation model that gives frames fro animations
 * @author Jan Svoboda-Fel-CVUT
 */
public class AnimEntity extends Entity{
    /** model loading the animation */
    protected transient AnimationModel animationModel; 
    /** direction where the entity is facing*/
    protected char direction;
    

    /**
     * Make an entity with hitbox, does not assign animation model
     * @param hitbox Hitbox of the entity (for collisions and movement)
     */
    public AnimEntity(Hitbox hitbox) {
        super(hitbox);
    }
    
    /**
     * set a new animation model for the entity
     * @param animationModel a new animations model
     */
    public void setAnimationModel(AnimationModel animationModel) {
        this.animationModel = animationModel;
    }
   
    /**
     * gets you a frame of current animation from the animation model based on the current time
     * @param currentMStime time of the system used to calculate current frame
     * @return current frame
     */
    @Override
    public Image whatFrame(double currentMStime){
        //to see, what frame of animation I should be on, or just the pic.
        return animationModel.getCurrentFrame(currentMStime);
    }
    
    /**
     * gets you the animation model of the entity
     * @return the animation model from the entity
     */
    public AnimationModel getAnimationModel() {
        return animationModel;    
    }    

    /**
     * change direction the entity is facing/moving
     * @param d new direction 
     */
    public void changeDir(char d){
        animationModel.turnToDir(d);
        direction=d;
    }
     
    /**
     * start an attack animation in a direction 
     * @param d direction of attack
     * @param currentMStime timestamp of the beginning of the attack
     */
    public void attackDir(char d,double currentMStime){
        System.out.println("AnimEntity registered attack to dir");
        animationModel.startAttacking(d,currentMStime);
        direction=d;
     }

    /**
     * move the entity in the direction where its currently moving.
     * these entities can move, not like the chest or the wall
     */
    @Override
    public void move() {
        hitbox.move(currentVelocity);
    }
     
     
    
}
