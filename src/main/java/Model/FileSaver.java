
package Model;

import Controller.MainController;
import Misc.Hitbox;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *Class that reads and writes the game data into a file
 * @author Jan Svoboda-Fel-CVUT
 */
public class FileSaver {
    private static final Logger logger = Logger.getLogger(FileSaver.class.getName());
    private final static String _SAVE_DEFAULT= "./src/main/resources/saves/default.save";
    private final static String _SAVE_SLOT= "./src/main/resources/saves/last.save";
    private MainController c;

    /**
     * default constructor of file saver
     * @param c controller that is saved
     */
    public FileSaver(MainController c) {
        this.c = c;
    }
    
    private void writeSave(String path){
        DataStorage storage = new DataStorage(); 
        storage.storeController(c);
 
        try (OutputStream fos = new FileOutputStream(path);
             ObjectOutputStream out = new ObjectOutputStream(fos)) {

            out.writeObject(storage);
        } catch (IOException e) {
            logger.log(Level.SEVERE,"Error while saving the storage class to file : "+e);
        }
    }
            
    private void readSave(String path){              
        try (   InputStream fis = new FileInputStream(path);
                ObjectInputStream in = new ObjectInputStream(fis)) {
            DataStorage storage = (DataStorage) in.readObject(); 
            storage.reloadSprites(c);
            c.fillDataFromStorage(storage);
        } catch (ClassNotFoundException e) {
           logger.log(Level.SEVERE,"Cant find the class definition: "+e);
        } catch (IOException e) {
           logger.log(Level.SEVERE,"Error while reading from a file : "+e);
        }
    }
    
    /**
     * save the current game data into the save slot
     */
    public void saveToSaveSlot(){
        logger.log(Level.INFO,"Saving to the save slot");
        writeSave(_SAVE_SLOT);
    }
    
    /**
     * save the current game data into the default slot
     */
    public void saveToDefault(){
        logger.log(Level.INFO,"Saving to the default slot");
        writeSave(_SAVE_DEFAULT);
    }
    
    /**
     * load the storage class from the save slot
     */
    public void readFromSaveSlot(){
        logger.log(Level.INFO,"reading from the save slot");
        readSave(_SAVE_SLOT);
    }
    
    /**
     * load the storage class from the default save slot
     */
    public void readFromDefault(){
        logger.log(Level.INFO,"reading from the default slot");
        readSave(_SAVE_DEFAULT);
    }

}
