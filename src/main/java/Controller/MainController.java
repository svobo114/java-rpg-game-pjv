

package Controller;

import Model.Entities.Individual.Player;
import Misc.Coord;
import Misc.Hitbox;
import Misc.Velocity;
import Model.DataStorage;
import Model.Entities.Entity;
import Model.Entities.Individual.Chest;
import Model.Inventory.EquipmentModel;
import Model.Inventory.Items.Individual.Key;
import Model.Inventory.Items.Item;
import View.AnimationModel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;

/**
 * Main controller class for the game. handles colisions, state updates and camera movement
 * @author Jan Svoboda-Fel-CVUT
 */
public class MainController extends Thread implements EventHandler<MouseEvent>{
    private static final Logger logger = Logger.getLogger(MainController.class.getName());
    private final static int _MOVEMENT_CONSTANT=3;
    private final static int _WINDOW_W=1179;
    private final static int _WINDOW_H=661;
    private final static int _MAP_W =3310; 
    private final static int _MAP_H =1748;
    private final static int _START_MAP_X=0;  private final static int _START_MAP_Y=0;
   
    private final static int _PLAYER_SPRITE_SIZE=64; //it is symmetrical
    private final static int _PLAYER_ATTAC_W=_PLAYER_SPRITE_SIZE;
    private final static int _PLAYER_ATTAC_H=_PLAYER_SPRITE_SIZE;
    private final static int _PLAYER_OFFSET_X=_WINDOW_W/2-32; //offset to the middle of the screen
    private final static int _PLAYER_OFFSET_Y=_WINDOW_H/2-32;
    private final static double _ATTAC_ANIM_LEN=0.4*2; //len*frames
    private final static int _CHEST_RANGE = 500;
    
    //variables
    private final List<String> input ;
    private long startNanoTime=0 ;
    
    //window positioning
    private Coord windowCoord;
    private Coord playerScreenCoord; 
    
    //player collisions handling 
    private Hitbox playerNextHitbox;
    private Hitbox playerHitbox;
    private final Velocity playerVelocity;
    
    //entity collision handling
    private List<Entity> listEntity; 
    private Hitbox tmpEntityHitbox;
    private Hitbox attackHitbox;
    
    private double time; //current time
    private double lastAttackTime=0;
    private char dir='d';
    
    private final Player pc;
    private final AnimationModel playerAnimation;
    private Boolean colX=false,colY=false;
    
    
    /**
     * makes a main controller and connects it to input list+ entity list
     * @param input shared input list
     * @param listEntity shared entity list
     */
    public MainController(List<String> input,List<Entity> listEntity) {
        logger.log(Level.INFO, "Initialising the main controller");
        this.input = input;
        this.listEntity=listEntity;
        this.playerAnimation=new AnimationModel("player");
        this.pc=new Player();
        
        //Movement and coord variables init
        playerVelocity=new Velocity(0,0); 
        windowCoord = new Coord(        _START_MAP_X,_START_MAP_Y );                      //starting map frame in the upper left, maximum is the map size
        playerHitbox=new Hitbox(        new Coord(_START_MAP_X+_PLAYER_OFFSET_X,_START_MAP_Y+_PLAYER_OFFSET_Y),      //starting position of player model on map
                                        _MAP_W,_MAP_H,      
                                        _PLAYER_SPRITE_SIZE,_PLAYER_SPRITE_SIZE);   
        playerNextHitbox=playerHitbox.returnCopy(); //duplicate 
        playerScreenCoord =new Coord( _PLAYER_OFFSET_X,_PLAYER_OFFSET_Y);                        //the place of player on the screen
        
        
    }
     
    /**
     * main thread functions, uses animation timer to time moving player and other entities
     */
    @Override
    public void run(){
        startNanoTime = System.nanoTime();
        new AnimationTimer()
        {
            @Override
            public void handle(long currentNanoTime){
                time = (currentNanoTime - startNanoTime) / 1000000000.0;
                boolean pressing=false;
                char newDir=dir;
                
                playerVelocity.zero();      //check input and act accordingly
                if (input.contains("W")){playerVelocity.y-=_MOVEMENT_CONSTANT;  newDir='u';pressing=true;}
                if (input.contains("A")){playerVelocity.x-=_MOVEMENT_CONSTANT;  newDir='l';pressing=true;}
                if (input.contains("D")){playerVelocity.x+=_MOVEMENT_CONSTANT;  newDir='r';pressing=true;}
                if (input.contains("S")){playerVelocity.y+=_MOVEMENT_CONSTANT;  newDir='d';pressing=true;}  
                if (pressing && newDir != dir){playerAnimation.startWalking(newDir,time); dir=newDir;} //I have actually changed directions
                
                
                //player moving inside map boundaries
                colX=moveCollision(new Velocity(playerVelocity.x,0));
                colY=moveCollision(new Velocity(0,playerVelocity.y));
                if (colX ){
                    playerVelocity.x=0;
                }
                if (colY ){
                    playerVelocity.y=0;
                }
                playerHitbox.move(playerVelocity);
                
                //window inside map positioning
                if (playerHitbox.leftUp.x > _PLAYER_OFFSET_X &&           //far from left edge
                      ( _MAP_W - playerHitbox.rightDown.x > _PLAYER_OFFSET_X )){   //far from right edge
                    windowCoord.x=playerHitbox.leftUp.x-_PLAYER_OFFSET_X;

                }
                if (playerHitbox.leftUp.y > _PLAYER_OFFSET_Y && 
                      ( _MAP_H - playerHitbox.rightDown.y > _PLAYER_OFFSET_Y)){ // half of screen on y from the top
                    windowCoord.y=playerHitbox.leftUp.y-_PLAYER_OFFSET_Y;
                }

                //calculate player on the window position
                playerScreenCoord.x=playerHitbox.leftUp.x-windowCoord.x;
                playerScreenCoord.y=playerHitbox.leftUp.y-windowCoord.y;
                
                /*
                System.err.println("------------------------------------");
                System.err.println("Velocity    X: "+playerVelocity.x+ "    Y: "+playerVelocity.y);
                System.err.println("Player Coords L: "+playerHitbox.leftUp.x+","+playerHitbox.leftUp.y);
                */
                
                for (Entity ent : listEntity) {
                    calculateMove(ent);
                }
                //calculate all other movements from velocities of mobs and move them accordingly
            }
        }.start();
    }

    /**
     *  used to calculate if the hitboxes colide when moving
     * @param vel - velocity of the hitbox that is being checked -> used to update the position
     * @return true if there is a collision with another hitbox
     */
    private boolean moveCollision(Velocity vel){
        playerNextHitbox.copyCoords(playerHitbox); 
        playerNextHitbox.move(vel); //moves soon to be coords
        for (Entity ent : listEntity) {//Check collisions with list of hitboxes all over map
            if (playerNextHitbox.doesCollide(ent.getHitbox())){
                return true;
            }
        }
        return false;
    }
    
    /**
     * calculates a position of the next hitbox
     * @param entity 
     */
    private void calculateMove(Entity entity){
        tmpEntityHitbox=entity.getHitbox().returnCopy(); 
        tmpEntityHitbox.move(entity.getCurrentMovement()); //moves soon to be coords
        for (Entity ent : listEntity) {//Check collisions with list of hitboxes all over map
            if (tmpEntityHitbox.doesCollide(ent.getHitbox()) && entity!=ent){
                return; //cant move, there is a collision with other mob
            }
        }
        if (tmpEntityHitbox.doesCollide(playerHitbox)){
                return; //cant move, there is a collision with player!
            }
        entity.move();
        
        
    }
    
    /**
     * gets yout the coordinate of the window on the map
     * @return Coord of left upper window corner
     */
    public synchronized Coord getWindowCoord() {
        return windowCoord.copy();
    }
    /**
     * Gets yout the coordinate of the player on your screen
     * @return Coord of players left upper corner
     */
    public synchronized Coord getPlayerScreenCoord() {
        return playerScreenCoord.copy();
    }
    
    /**
     * gets you the cordinate of player on the map 
     * @return left upper coord of players hitbox in relation to the map
     */
    public synchronized Coord getPlayerMapCoord() {
        return playerHitbox.leftUp.copy();
    }
    
    /**
     * Gets the players current animation frame based on the thecurrent time
     * @param startTime the current time of the system, that tells the system which frame to pick
     * @return Image 64x64 which is the current animation frame of the player
     */
    public synchronized Image getPlayerSprite(double startTime) {
        return playerAnimation.getCurrentFrame(startTime);
    }
    
    /**
     * Gets players health in percentage of maximum
     * @return integer meaning % of max health
     */
    public int getPlayerHealthPercentage(){
        return ((playerHitbox.width * pc.getCurrentHealth())/pc.getMaxHealth());  
    }
    
    /**
     * Gets players mana in percentage of maximum
     * @return integer meaning % of max mana
     */
    public int getPlayerManaPercentage(){
        return ((playerHitbox.width * pc.getCurrentMana())/pc.getMaxMana()); 
    }
    
    /**
     * Gets you the money that player currently has
     * @return amount of players money
     */
    public int getPlayerMoney(){
        return pc.getCurrentMoney();  
    }
    
    /**
     * gets you players current health 
     * @return integer meaning health amount
     */
    public int getPlayerCurrentHealth(){
        return pc.getCurrentHealth();
    }
    
    /**
     * gets you the players current mana
     * @return gets you integer meaning mana amount
     */
    public int getPlayerCurrentMana(){
        return pc.getCurrentMana();    
    }
    

    /**
     * handle for clicking to cause an attack
     * @param t MouseEvent that sets up either attack or alternative button
     */
    @Override
    public void handle(MouseEvent t) { //mouse events from game stage/scene
        if(t.isPrimaryButtonDown()){
            System.out.println("M1 pressed");
            playerAnimation.startAttacking(dir, time);
            attack(dir);
        }
        if(t.isSecondaryButtonDown()){
            System.out.println("M2 pressed");
            //use quickslot or something
        }
    }
    
    /**
     * Function handling a players attack based on current direction
     * @param dir (direction to which player attacks
     */
    private void attack(char dir){
        if (time-lastAttackTime<_ATTAC_ANIM_LEN){ //to see if the attack ended, so I can't just spam 
            return;
        }
        int att=pc.swingWeapon();
        switch (dir) {
            case 'r':
                attackHitbox=new Hitbox(new Coord(playerHitbox.rightDown.x,playerHitbox.leftUp.y),_PLAYER_ATTAC_W,_PLAYER_ATTAC_H);
                break;
            case 'l':
                attackHitbox=new Hitbox(new Coord(playerHitbox.leftUp.x-_PLAYER_ATTAC_W,playerHitbox.leftUp.y),_PLAYER_ATTAC_W,_PLAYER_ATTAC_H);
                break;
            case 'u':
                attackHitbox=new Hitbox(new Coord(playerHitbox.leftUp.x,playerHitbox.leftUp.y-_PLAYER_ATTAC_H),_PLAYER_ATTAC_W,_PLAYER_ATTAC_H);
                break;
            case 'd':
                attackHitbox=new Hitbox(new Coord(playerHitbox.leftUp.x,playerHitbox.rightDown.y),_PLAYER_ATTAC_W,_PLAYER_ATTAC_H);
                break;
            default:
                return; //bad direction, error somewhere
        }
        //collision ?
        for (Entity ent : listEntity) {
            if (attackHitbox.doesCollide(ent.getHitbox())){
                //successfull hit
                ent.getAttacked(att,0);
            }
        }
        lastAttackTime=time;
    }
    
    
    /**
     * Gives the player "amount" of money
     * @param amount - money to give to player
     */
    public void giveMoney(int amount){
        pc.addMoney(amount);
    }
    
    /**
     * Kill entity from the entity list
     * @param ent - instance of entity to kill
     */
    public void killEnt(Entity ent){
        // Possible next step: spawn corpse
        listEntity.remove(ent);
    }

    /**
     * Gets you the players current inventory list
     * @return List of items that player has in his inventory
     */
    public List<Item> getPlayerInv() {
        return pc.getInventory();
    }
    
    /**
     * Gets you the players current hitbox
     * @return Hitbox used for players collisions
     */
    public Hitbox getPlayerHB() {
        return playerHitbox;
    }
    
    /**
     * gets you the list of entities that are on the map
     * @return List of entities (that are on the map)
     */
    public List<Entity> getEntities(){
        return listEntity;
    }
    
    /**
     * Gets you the players equipment model with armor and current equiped weapon
     * @return model of equipment from player
     */
    public EquipmentModel getPlayerEquipmentmodel(){
        return pc.getEquipmentModel();
    }
    
    /**
     * gets you the whole player model, with its values, equipment and inventory
     * @return player model used in controller
     */
    public Player getPlayerModel(){
        return pc;
    }
    
    /**
     * Gives the player character a new item in his inventory
     * @param e item to give to inventory
     */
    public void givePlayerItem(Item e){
        pc.additem(e);
    }
    
    /**
     * Looks for the closest chest in range _CHEST_RANGE
     * @return Chest that is nearby enough to open
     */
    public Chest lookForChest(){     
        for (Entity ent: listEntity) {
            logger.log(Level.FINE, "CHECKING entity for chest: "+ ent.getClass()+"  isChest: "+ (ent instanceof Chest) );
            if (ent instanceof Chest && ent.getHitbox().isInRange(getPlayerMapCoord(), _CHEST_RANGE)){
                logger.log(Level.INFO, "Chest found, LOOKING for key");
                if (lookForKey((Chest) ent) ){
                    return (Chest) ent;
                }
            }  
        }
        logger.log(Level.INFO, "No OPENABLE Chest found");
        return null;
    }
    
    /**
     * Looks for a key in the players inventory, based on a particular chest to open
     * @param ch Chest that needs a key
     * @return true, if the key for the chest has been found
     */
    private boolean lookForKey(Chest ch){
        List<Item> invTmp=getPlayerInv();
        if (invTmp.isEmpty()){return false;}
        Key tmp=null;
        
        for ( var it : invTmp){
            if (it instanceof Key){
                tmp =(Key)it;
            }else {
                continue;
            }
            
            if (ch.checkKey(tmp.getKey())){
                logger.log(Level.INFO, "FOUND a key: {0}", tmp.getKey());
                return true;
            } 
        }
        logger.log(Level.INFO, "key does NOT work");
        return false;
    }
   
    /**
     * Given a storage, this function fills the controller back to previous state, then fills the player model
     * @param storage Storage class that is being used to fill this controller. loaded from a file
     */
    public void fillDataFromStorage(DataStorage storage){
        this.playerHitbox=storage.getPlayerHitbox();
        this.listEntity=storage.getListEntity();
        this.playerScreenCoord=storage.getPlayerScreenCoord();
        this.windowCoord=storage.getWindowCoord();
        this.pc.fillFromStorage(storage.getPlayerHealth(), storage.getPlayerMana(),storage.getPlayerGold(), storage.getPlayersInv(), storage.getPlayersEq());
    }
    
}
