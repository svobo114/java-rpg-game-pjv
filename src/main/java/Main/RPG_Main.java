package Main; 

import View.WindowView;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *  Main class for project RPG Game. Semestral project for Java
 * @author Jan Svoboda-Fel-CVUT
 */
public class RPG_Main extends Application{
    private static final Logger logger = Logger.getLogger("Main Logger");
    private WindowView graphics;

    /**
     * starts the application of java rpg game made with FX
     * @param args arguments fro the main
     */
    public static void main(String[] args) {
        logger.log(Level.FINE, "Running Main");
        launch();
    }
    

    @Override
    public void start(Stage stage) throws Exception {
        try {
            graphics = new WindowView();
        }
        catch(Exception e){
            logger.log(Level.SEVERE, "Problem initialising graphics");
            System.exit(-2);
        }
        
        graphics.playGame();
        logger.log(Level.INFO, "Game window Launched");
    }
    
    
    

}
