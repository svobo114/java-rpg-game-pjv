

package View;

import Model.Inventory.Items.Item;

/**
 * interface for moving items inside different scenes handling inventories
 * @author Jan Svoboda-Fel-CVUT
 */
public interface InventoryListener {

    /**
     * Handles how items are moved inside different inventory scenes
     * @param item Which item is being moved
     * @param inInv If it is in the inventory side of the equipment side of the scene (or generally "other" side)
     */
    void handleItemClick(Item item,boolean inInv);
}
