

package View;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;

/**
 * Class holding animation data for multiple animations ans switching between them
 * @author Jan Svoboda-Fel-CVUT
 */
public class AnimationModel {
    private static final Logger logger = Logger.getLogger(AnimationModel.class.getName());
    private double _ANIM_LEN=0.4;
    private int _FRAME_NUM=2;
    private String name; 
    private double timeOfTheAnimationStart;
    
    private AnimatedImage attackU,attackD,attackL,attackR; 
    private AnimatedImage walkU,walkD,walkR,walkL; //different walk animations for different sides
    private AnimatedImage currentAnimation;
    private AnimatedImage currentIdleAnimation; 
    //in this case I use walk animations like idle animations, but there is now reason why there cant be a walk animation (I just dont have any frames for it)
    
    

    /**
     * makes an animation model based on input parameters
     *  require name of the animation to be 
     *  path/attack/attack0.png
     * @param name name of the whole package in resources
     * @param len length of animation
     * @param frames how many frames does the animation have.
     */
    public AnimationModel( String name,double len, int frames){
        this.name=name;
        setAttack(name);
        setWalking(name);
        currentAnimation=walkD; //idle
        currentIdleAnimation=walkD;
        
        _ANIM_LEN=len;
        _FRAME_NUM=frames;
    }
    
    /**
     * makes an animation model based on input parameters
     *  Without setting length and frames -> set based on base values 
     * @param name meaning the 
     */
    public AnimationModel( String name){
        this.name=name;
        setAttack(name);
        setWalking(name);
        currentAnimation=walkD;
        currentIdleAnimation=walkD;
    }

    private void setAttack(String name){
        String tmp="attack";
        logger.log(Level.FINE, "fetching a pic from: "+name+"/"+tmp+"/"+tmp+"U -------------------------");
        attackU= new AnimatedImage(_FRAME_NUM,_ANIM_LEN,name+"/"+tmp+"/"+tmp+"U",false);
        attackD= new AnimatedImage(_FRAME_NUM,_ANIM_LEN,name+"/"+tmp+"/"+tmp+"D",false);
        attackL= new AnimatedImage(_FRAME_NUM,_ANIM_LEN,name+"/"+tmp+"/"+tmp+"L",false);
        attackR= new AnimatedImage(_FRAME_NUM,_ANIM_LEN,name+"/"+tmp+"/"+tmp+"R",false);
    }
    private void setWalking(String name){
        String tmp="walk";
        logger.log(Level.FINE, "fetching a pic from: "+name+"/"+tmp+"/"+tmp+"U -------------------------");
        walkU= new AnimatedImage(_FRAME_NUM,_ANIM_LEN,name+"/"+tmp+"/"+tmp+"U",true);
        walkD= new AnimatedImage(_FRAME_NUM,_ANIM_LEN,name+"/"+tmp+"/"+tmp+"D",true);
        walkL= new AnimatedImage(_FRAME_NUM,_ANIM_LEN,name+"/"+tmp+"/"+tmp+"L",true);
        walkR= new AnimatedImage(_FRAME_NUM,_ANIM_LEN,name+"/"+tmp+"/"+tmp+"R",true);
    }
    

    /**
     * gets you the current frame of animation which is now being played
     * @param time timestamp to extrapolate current animation frame
     * @return returns image for current animation frame
     */
    public Image getCurrentFrame(double time) {
        //logger.log(Level.INFO, "Inside Model - time: "+time+ ", start of this animation " +timeOfTheAnimationStart+ ", current: "+ currentAnimation.toString() );
        if( isNextAnimation(time)){
            //System.out.println("time to switch to next animation");
            currentAnimation=currentIdleAnimation; //after end of attack
            timeOfTheAnimationStart=time; //setting animation back to Idle and reseting time on it    
        }
        return currentAnimation.getFrame(time-timeOfTheAnimationStart); //will get frame 0 if animation reset
    }
    
    /**
     * Changes the animation when turning to other direction
     * @param dir direction to turn to
     */
    public void turnToDir(char dir){
        currentIdleAnimation = switch (dir) {
            case 'r' -> walkR;
            case 'u' -> walkU;
            case 'l' -> walkL;
            default -> walkD;
        };
    }
    
    /**
     * Starts a walking animation from any previous animation
     * @param dir direction of walking
     * @param currentTime current time stamp
     */
     public void startWalking(char dir,double currentTime){
        if(currentAnimation.isOverWriteble() || isNextAnimation(currentTime)){       
            //logger.log(Level.INFO,"starting animation to dir: "+dir);
            currentAnimation = switch (dir) {
                case 'r' -> walkR;
                case 'u' -> walkU;
                case 'l' -> walkL;
                default -> walkD;
            };
            currentIdleAnimation=currentAnimation;
            timeOfTheAnimationStart=currentTime;
        }else{
            //just turn, dont change the animation yet!
            turnToDir(dir);
        }  
    }
     
     /**
      * suspend any current animation and start an attack animation
      * @param dir direction of attack
      * @param currentTime timetamp of start of attack
      */
    public void startAttacking(char dir,double currentTime){
        //System.out.println("attacking to dir: "+dir);
        currentAnimation = switch (dir) {
            case 'r' -> attackR;
            case 'u' -> attackU;
            case 'l' -> attackL;
            default -> attackD;
        };
        timeOfTheAnimationStart=currentTime;
    }
    
    /**
     * Looks if the animation has ended
     * @param currentTime current time stamp (to evaluate the animation length)
     * @return true if  the current animation is ending and its time to switch to default
     */
    private boolean isNextAnimation(double currentTime){
        return currentTime-timeOfTheAnimationStart>currentAnimation.getDurationAll();
    }
    
}
