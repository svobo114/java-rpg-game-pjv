

package View;

import java.util.List;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import Model.Inventory.Items.Item;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class used as a factory to build an inventory panel reused inside GUI
 * @author Jan Svoboda-Fel-CVUT
 */
public class InventoryPanel extends GridPane {
    private static final Logger logger = Logger.getLogger(InventoryPanel.class.getName());
    private final static int _INV_BUTTON_ROW=4;
    //for inventory panels
    private static BorderStroke bss=new BorderStroke(Color.BLACK,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,BorderStroke.THICK);
    private InventoryListener listener;
    private List<Button> windows=new ArrayList<>();
    private List<Item> inv;

    /**
     * makes an inventory panel
     * @param listener Listener that handles clicking items inside the inventory
     */
    public InventoryPanel(InventoryListener listener) {
        this.listener = listener;
        this.build();
    }    
    
    private void build(){ 
        this.setVgap(5);
        this.setHgap(5);
        this.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(25,25,25,25));
        for (int i = 0; i < _INV_BUTTON_ROW*_INV_BUTTON_ROW;i++) {
            Button button=new Button();
            button.setPrefSize(125, 125);
            button.setBorder(new Border(bss));
            button.setBackground(Background.EMPTY);
            //button.setOnMouseClicked(listener);//sending to listener?
            button.setOnMouseClicked(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent me)
                {
                    logger.log(Level.FINE,"clicked an inventory button");
                    //action with item
                    Item tmp=findItem(inv,button.getBackground());
                    if (tmp!=null){
                       listener.handleItemClick(tmp,true); 
                    }
                    
                }
            });
            button.setOnMouseEntered(
            new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent me)
                {
                    logger.log(Level.FINE,"entered a button");
                }
            });
         button.setOnMouseExited(
            new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent me)
                {
                    logger.log(Level.FINE,"exited a button");
                }
            });
            //Here add handling for using items
            
            GridPane.setConstraints(button,i%_INV_BUTTON_ROW, i/_INV_BUTTON_ROW);
            this.getChildren().add(button);   
            windows.add(button);
        }
    }
    
    private void setInventoryPanelIcon(Button button, Background bg){
        button.setBackground(bg);
    }
    
    /**
     * Method shows inventory that it recieves in the fields on inventory panel
     * @param inventory List of items to display in the panel of buttons
     */
    public void showInventory(List<Item> inventory){
        int i =0;
        inv=inventory;
        for (Button window : windows) {
            window.setBackground(Background.EMPTY);
        }
        for (Button window : windows) {
            if (i < inventory.size()){
               setInventoryPanelIcon(window, inventory.get(i).getSprite()); 
            }else { break;}
            i++;
        }
    }
    
    /**
     * Can find an Item in the inventory based on the background of (clicked on) item
     * @param inventory inventory to search through
     * @param BG Background to find item by
     * @return Item which has this background from List of items (inventory)
     * returns null when the item is not found in the inventory
     */
    public Item findItem(List<Item> inventory,Background BG){
        for (Item item:inventory){
            if (item.getSprite()==BG){
                return item;
            }
        }
        logger.log(Level.SEVERE, "Item not found based on the BG in inventory");
        return null;
    }
    
    

}
