package View;


import Controller.MainController;
import Model.Entities.Entity;
import Model.Entities.Individual.Chest;
import Model.Entities.Individual.Skeleton;
import Model.Entities.Individual.Slime;
import Model.Entities.Individual.Tree;
import Model.Entities.Individual.Wall;
import Model.FileSaver;
import Model.Inventory.Items.Individual.Armour1;
import Model.Inventory.Items.Individual.Armour2;
import Model.Inventory.Items.Individual.BestSword;
import Model.Inventory.Items.Individual.Key;
import Model.Inventory.Items.Individual.MeeleDagger;
import Model.Inventory.Items.Item;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import static java.util.logging.Level.FINE;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


/**
 *  This class holds the game stage (window) and switches between scenes inside it
 * @author Jan Svoboda-Fel-CVUT
 */
public class GameSceneMaster extends Stage {
    private final static int _WINDOW_W=1179;
    private final static int _WINDOW_H=661;
    private static final Logger logger = Logger.getLogger(GameSceneMaster.class.getName());

    private GraphicsUpdateThread animationThread;
    private MainController controller;
            
    //drawing on canvas in game
    private Canvas gameCanvas;
    private GraphicsContext gc;
    private List<String> input = new CopyOnWriteArrayList<>();
    private List<Entity> listEntity= new CopyOnWriteArrayList<>();

    //pictues
    private Background invPanelBg;  //472 342 used for every bg except game
    private Background hoverButton;
    private Background idleButton;
    
    private Scene gameScene;
    private OptionsScene optionsScene;
    private InventoryScene inventoryScene; 
    private ChestScene chestScene;
    private Scene youDiedScene; //currently not in use, since u cant really die?

    /**
     * makes a new scene master and loads backgrounds
     */
    public GameSceneMaster() {
        logger.log(Level.INFO, "Initiating the game window master");
        
        this.setTitle("RPG_Game - Game Window");
        this.setHeight(_WINDOW_H);
        this.setWidth(_WINDOW_W);
        this.setResizable(false);
           
        //Images and BGs
        Image imgPanel=null;
        Image imgButtonIdle = null;
        Image imgButtonHover = null;
        try {
                imgPanel = new Image("backgrounds/optionsBG.png");                
                imgButtonIdle = new Image("ui/buttonIdle.png");
                imgButtonHover = new Image("ui/buttonHover.png");
                
        }catch (Exception e){
            logger.log(Level.SEVERE, "Unable to load some Resource for GameView, Shutting down: {0}", e.toString());
            System.exit(-1);
        }
        BackgroundImage tmp;
        tmp = new BackgroundImage(imgPanel,BackgroundRepeat.SPACE, BackgroundRepeat.SPACE, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        invPanelBg=new Background(tmp);
        tmp = new BackgroundImage(imgButtonIdle, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        idleButton = new Background(tmp);
        tmp = new BackgroundImage(imgButtonHover, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        hoverButton = new Background(tmp);
        
        try{ 
            //build all the individual scenes
            buildGame(); 
            buildInv();
            buildOpt();
            buildChest();
            
        }catch (Exception e){
            logger.log(Level.SEVERE, "Error bulding gameView Scenes{0}", e);
            System.exit(-3);
        }
    }
    
    /**
     * Start running all the threads
     * CAN initiate entities
     */
    public void startGameView(){
        logger.log(Level.INFO, "Starting the game view");
        controller.start();
        animationThread.start();
        //initEntities(); 
                        /*
                        comment out, if you want to only load entities from a file.
                        if you have this uncommented, you STILL overwrite the entities when loading the save UNLESS there si an error 
                        If you want to edit entities->  uncomment this line (129), 
                                                        comment "gameView.loadDefault(); " (line 96 in WindowView) (so you dont overwrite the entitiy list)
                                                        edit "initEntities" function lower to change what entities spawn on the map
                                                        save in game to make a new save
                                                        change the name of the new save ("last.save") to "default.save" to move it to a default save 
                                                            OR just load the last game next time to hop back in
                        */  
        this.show();
    }
    
    private void buildInv(){
        inventoryScene=new InventoryScene(invPanelBg, controller, new HBox());
            inventoryScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent e) {
                        String code = e.getCode().toString();
                        System.out.println("Pressing input "+ code + " in the Inventory Scene");
                        if ( code.equals("I")){
                            showInventoryScene();
                        } else if (code.equals("ESCAPE")){
                            showOptionsScene();
                        }
                    }
                });
    }
      
    private void buildOpt(){
        Button button1 = new Button("Resume");   
        Button button2 = new Button("Save Game");
        Button button3 = new Button("Quit");

        setButtonProperties(button1);
        setButtonProperties(button2);
        setButtonProperties(button3);
        
        optionsScene=new OptionsScene(invPanelBg,this,new VBox(),button1,button2,button3);
    }
   
    private void buildGame(){ 
        gameCanvas = new Canvas(_WINDOW_W,_WINDOW_H);
        gc = gameCanvas.getGraphicsContext2D();
        
        controller=new MainController(input, listEntity );
        
        animationThread=new GraphicsUpdateThread(gc,controller,listEntity);
        gameScene=new Scene(new Group(gameCanvas)); //not naming the group, because it does not matter
        this.setScene(gameScene);
        
        //event listeners for game scene, where canvas is
        gameScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent e) {
                String code = e.getCode().toString();
                //System.err.println("Pressing input "+ code + " in the main game window");
                if ( code.equals("I")){
                    showInventoryScene();
                }
                else if ( code.equals("ESCAPE")){
                    showOptionsScene();
                }else if ( code.equals("E")){
                    Chest tmp = controller.lookForChest();
                    if (tmp != null ){
                        chestScene.changeInv(tmp);
                        showChestScene();
                    }
                }
                else if ( !input.contains(code) ){
                    input.add( code );
                }
            }
        });
        gameScene.setOnKeyReleased((KeyEvent e) -> {
            String code = e.getCode().toString();
            input.remove( code );
        });

        gameScene.addEventHandler( MouseEvent.MOUSE_PRESSED,controller);
    }
    
    private void buildChest(){
        chestScene=new ChestScene(invPanelBg,controller,this,new VBox());
    }
    
    /**
     * change scene to show the inventory
     */
    public void showInventoryScene(){
        if (this.getScene()==inventoryScene){//inventory is showing
            this.setScene(gameScene);
        }else{
            System.err.println("action: open inventory");
            input.clear();//no input shall prevail
            inventoryScene.refresh();
            this.setScene(inventoryScene);
        } 
    }
    
    /**
     * change scene to show the options scene
     */
    public void showOptionsScene(){
        if (this.getScene()==optionsScene){//options is showing
            this.setScene(gameScene);            
        }else{
            System.err.println("action: open options");
            input.clear();//no input shall prevail
            this.setScene(optionsScene);
        }
    }
    
    /**
     * change scene to open the chest nearby
     */
    public void showChestScene(){
        if (this.getScene()== chestScene){
            this.setScene(gameScene);
        }else if (this.getScene() == gameScene){
            this.setScene(chestScene);            
            logger.log(FINE,"action: open chest");
            input.clear();//no input shall prevail
        } 
    }

    private void setButtonProperties(Button button){
        button.setPrefSize(165, 70);
        button.setBackground(idleButton);
        button.setTextFill(Color.WHITE);
        button.setOnMouseEntered(
            new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent me)
                {
                    button.setBackground(hoverButton);
                }
            });
         button.setOnMouseExited(
            new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent me)
                {
                    button.setBackground(idleButton);
                }
            });
    }
    
    
    private void addEntity(Entity e){
        listEntity.add(e);
   }
    
    private void initEntities(){
        addEntity(new Wall(1200,500, 500, 150 )); //x,y,w,h
        addEntity(new Wall(1400,600, 75, 500 )); 
        addEntity(new Tree(700, 1200));//x,y
        addEntity(new Tree(2400, 300));
        addEntity(new Tree(1700, 1400));
        addEntity(new Slime(500,800,controller));
        addEntity(new Slime(1300,1200,controller));
        addEntity(new Slime(620,1100,controller));
        addEntity(new Skeleton(300,800,controller));
        addEntity(new Skeleton(2500,1400,controller));
        
        List<Item> chestInv1=new CopyOnWriteArrayList<>();
        chestInv1.add(new Armour1());
        chestInv1.add(new MeeleDagger());
        chestInv1.add(new Key("chest2"));
        addEntity(new Chest(1100,650,"chest1",chestInv1));
        
        List<Item> chestInv2=new CopyOnWriteArrayList<>();
        chestInv2.add(new Armour2());
        chestInv2.add(new BestSword());
        addEntity(new Chest(300,420,"chest2",chestInv2));
    }
    
    /**
     * Save from the inside of the game 
     * saves from the classes currently running to save slot
     */
    public void saveFromGame(){
        FileSaver saver =new FileSaver(controller);
        saver.saveToSaveSlot();
    }
    
     /**
     * Save from the inside of the game 
     * saves from the classes currently running to the default slot
     * USE ONLY FOR DEBUGGING to rewrite the basic save when making changes!
     */
    public void superSecretSaveFromGameToDefault(){
        FileSaver saver =new FileSaver(controller);
        saver.saveToDefault();
    }

    /**
     * Loads the classes for running the game 
     * from a file "last.save" which holds the last save
     */
    public void loadSave() {
        FileSaver saver = new FileSaver(controller);
        saver.readFromSaveSlot();
        animationThread=new GraphicsUpdateThread(gc,controller,controller.getEntities());
    }
    
    /**
     * Loads the classes for running the game
     * from a file "default.save" which holds the data about the default save
     */
    public void loadDefault() {
        FileSaver saver = new FileSaver(controller);
        saver.readFromDefault();
        animationThread=new GraphicsUpdateThread(gc,controller,controller.getEntities());
    }

    
    
}
