package View;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


/**
 *  Main view class
 *  Has access to all the windows and shows them when needed
 * @author Jan Svoboda-Fel-CVUT
 */
public class WindowView {
    private static final Logger logger = Logger.getLogger(WindowView.class.getName());
    //windows used for the game
    private Stage mainMenuWindow=null;
    private Scene mainMenuScene,loadingScene;
    private GameSceneMaster gameView;
    
    //images used for the menu
    private Background hoverButton;
    private Background idleButton;
    private Background menuPicture;
    

    /**
     * constructor loads image resources and calls all init 
     */
    public WindowView() {
        //loading images for main menu
            Image imgMenu = null;
            Image imgButtonIdle = null;
            Image imgButtonHover = null;
            try {
                imgMenu = new Image("backgrounds/menuBG.png");
                imgButtonIdle = new Image("ui/buttonIdle.png");
                imgButtonHover = new Image("ui/buttonHover.png");

            } catch (Exception e){
                logger.log(Level.SEVERE, "Unable to load some Resource for Main Menu, Shutting down: " + e.toString());
                System.exit(-1);
            }
            
            BackgroundImage bgMenu= new BackgroundImage(imgMenu, BackgroundRepeat.SPACE, BackgroundRepeat.SPACE, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
            menuPicture = new Background(bgMenu);
            BackgroundImage bgButtonIdle = new BackgroundImage(imgButtonIdle, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
            idleButton = new Background(bgButtonIdle);
            BackgroundImage bgButtonHover = new BackgroundImage(imgButtonHover, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
            hoverButton = new Background(bgButtonHover);
        
        //prepare all windows
        gameView=new GameSceneMaster();
        prepareMainMenu();       
        
    }
    
    /**
     * prepares main menu with all the buttons and event handling
     */
    private void prepareMainMenu(){
        logger.log(Level.INFO, "Starting preparing Main menu");
        mainMenuWindow =new Stage();
        mainMenuWindow.setTitle("RPG_Game - Main Menu");
        mainMenuWindow.setHeight(600);
        mainMenuWindow.setWidth(800);
        mainMenuWindow.setResizable(false);
        
        Button button1 = new Button("New Game");   
        Button button2 = new Button("Load Last Save");
        Button button3 = new Button("Quit");

        setButtonProperties(button1);
        setButtonProperties(button2);
        setButtonProperties(button3);
        
        button1.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent me)
            {
                logger.log(Level.INFO, "Loading the basic level");
                gameView.loadDefault(); //comment this when you want to leave entities from the initEntities in GameSceneMaster
                gameView.startGameView();
                mainMenuWindow.hide();
            }
        });
        button2.setOnMouseClicked(
        new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent me)
            {
                logger.log(Level.INFO, "Loading the last saved level");
                gameView.loadSave();
                gameView.startGameView();
                mainMenuWindow.hide();
            }
        });
        button3.setOnMouseClicked(
        new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent me)
            {
                System.exit(0);
            }
        });

        VBox container = new VBox();
        container.getChildren().addAll(button1,button2,button3);
        container.setSpacing(25);
        container.setPadding(new Insets(25,25,25,25));
        container.setAlignment(Pos.CENTER);
        container.setBackground(menuPicture);
        mainMenuScene=new Scene(container);
        mainMenuWindow.setScene(mainMenuScene);
        logger.log(Level.FINE, "Main menu Prepared");
    }
              
    /**
     * method makes the main menu visible. called in main
     */
    public void playGame(){
        mainMenuWindow.show();
    }
        

    private void setButtonProperties(Button button){
        button.setPrefSize(165, 70);
        button.setBackground(idleButton);
        button.setTextFill(Color.WHITE);
        button.setOnMouseEntered(
            new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent me)
                {
                    button.setBackground(hoverButton);
                }
            });
         button.setOnMouseExited(
            new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent me)
                {
                    button.setBackground(idleButton);
                }
            });
    }

}
