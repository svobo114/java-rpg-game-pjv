

package View;

import Controller.MainController;
import Model.Inventory.EquipmentModel;
import Model.Inventory.Items.Item;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * Class used as a factory to build a whole inventory scene
 * The scene is to be used in main game stage to display inventory and equipment of the player
 * @author Jan Svoboda-Fel-CVUT
 */
public class InventoryScene extends Scene implements InventoryListener{
    private static final Logger logger = Logger.getLogger(InventoryScene.class.getName());
    private Background invPanelBg;
    private Background slotArmorBG;
    private Background slotWeaponBG;
    private Background slotQuickBG;
    private Item armor=null;
    private Item weapon=null;
    private Item quick=null;
    
    private BorderStroke bss=new BorderStroke(Color.BLACK,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,BorderStroke.THICK);
    
    //inside equipment panel
    private Button buttonArmor;
    private Button buttonWeapon;
    private Button buttonQuick;
    
    private HBox inventoryBox;//the HBox containing the equipment and the inventory panels
    private VBox equipment;
    private InventoryPanel inventoryPanel;
    private MainController controller;
    
    /**
     * Makes a new inventory scene based on background, connection to handler and a HBox to be the scene main group
     * @param panelBG background of the scene
     * @param c main controller that handles the items
     * @param inventoryBox that is used as a group for graphics class
     */
    public InventoryScene(Background panelBG, MainController c,HBox inventoryBox) {
        //make inventory panel, make equipment, put it together into HBox and set it as a scene root
        super(inventoryBox);
        this.inventoryBox=inventoryBox;
        this.controller=c;
        equipment=new VBox();
        inventoryPanel=new InventoryPanel(this);
        //add these into inventoryBox
        
        //Images and BGs
        invPanelBg=panelBG;
        Image imgWeapon = null;
        Image imgQuick = null;
        Image imgArmor = null;
        try {
            imgWeapon = new Image("ui/weaponSlot.png");
            imgQuick = new Image("ui/quickSlot.png");
            imgArmor = new Image("ui/armorSlot.png");
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Unable to load some Resource for GameView, Shutting down: {0}", e.toString());
            System.exit(-1);
        }
        BackgroundImage tmp;
        tmp = new BackgroundImage(imgQuick, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        slotQuickBG= new Background(tmp);
        tmp = new BackgroundImage(imgArmor, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        slotArmorBG = new Background(tmp);
        tmp = new BackgroundImage(imgWeapon, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        slotWeaponBG = new Background(tmp);
        
        //build all
        buildEquipment();
        buildInventoryBox();
    }
    
    private void buildInventoryBox(){     
        inventoryBox.setSpacing(40);
        inventoryBox.getChildren().addAll(inventoryPanel,equipment);
        inventoryBox.setBackground(invPanelBg);
        inventoryBox.setAlignment(Pos.CENTER);
    }
    
    
    private void buildEquipment (){ 
        equipment.setAlignment(Pos.CENTER);
        equipment.setPadding(new Insets(25,25,25,25));
        equipment.setSpacing(40);

        buttonArmor=new Button();
        buttonWeapon=new Button();
        buttonQuick=new Button();
        buttonArmor.setPrefSize(150, 150);
        buttonWeapon.setPrefSize(150, 150);
        buttonQuick.setPrefSize(150, 150);
        
        buttonArmor.setBackground(slotArmorBG);
        buttonWeapon.setBackground(slotWeaponBG);
        buttonQuick.setBackground(slotQuickBG);
        
        buttonArmor.setBorder(new Border(bss));
        buttonWeapon.setBorder(new Border(bss));
        buttonQuick.setBorder(new Border(bss));
        buttonArmor.setOnMouseClicked(
            new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent me)
                {
                    handleItemClick(armor,false);
                }
            });
        buttonWeapon.setOnMouseClicked(
            new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent me)
                {
                    handleItemClick(weapon,false);
                }
            });
        buttonQuick.setOnMouseClicked(
            new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent t) {

                }
            });

        equipment.getChildren().addAll(buttonArmor,buttonWeapon,buttonQuick);
        
    }

    /**
     * show players current inventory in the inventory panel
     */
    public void showInv(){
        inventoryPanel.showInventory(controller.getPlayerInv());
    }
    
    /**
     * Show players current equipment in the equipment panel
     */
    public void showEquip(){
        EquipmentModel model=controller.getPlayerEquipmentmodel();
        armor=model.myArmor;
        weapon=model.myWeapon;
        buttonArmor.setBackground(armor.getSprite());
        buttonWeapon.setBackground(weapon.getSprite());
    }
    
    /**
     * refresh the panel based on players most recent inventory and equipment
     */
    public void refresh(){
        showInv();
        showEquip();
    }

    @Override
    public void handleItemClick(Item item,boolean inInv) {
       if(inInv){
           controller.getPlayerModel().equipItem(item);
       }else{
           controller.getPlayerModel().deEquipItem(item);
       }
       refresh();

    }

}
