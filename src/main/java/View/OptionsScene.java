

package View;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.VBox;

/**
 * Scene class used in main game window to show in-game options
 * @author Jan Svoboda-Fel-CVUT
 */
public class OptionsScene extends Scene {
    private static final Logger logger = Logger.getLogger(OptionsScene.class.getName());

    private VBox optionsPanel;
    private GameSceneMaster handler; //in this case, I need to just handle key presses, not mouse events
    
    private Button b1,b2,b3;

    /**
     * Makes a new options scene
     * @param panelBG background for the scene
     * @param handler game scene master used to switch scenes
     * @param optionsPanel VBox that will hold the buttons
     * @param b1 button 1
     * @param b2 button 2
     * @param b3 button 3
     */
    public OptionsScene(Background panelBG, GameSceneMaster handler,VBox optionsPanel,Button b1,Button b2,Button b3) {
        super(optionsPanel);
        this.optionsPanel = optionsPanel;
        this.handler=handler;
        
        this.b1=b1;
        this.b2=b2;
        this.b3=b3;
        
        //new button that is hidden BUT saves into default save slot - for debugging
        Button tmp=new Button();
        tmp.setBackground(null);
        tmp.setPrefSize(100, 50);
        
        optionsPanel.setBackground(panelBG);
        optionsPanel.setSpacing(60);
        optionsPanel.setAlignment(Pos.CENTER);
        
        optionsPanel.getChildren().addAll(b1,b2,b3, tmp);
        
        b1.setOnMouseClicked(
        new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent me)
            {
                handler.showOptionsScene();
            }
        });
        b2.setOnMouseClicked(
        new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent me)
            {
                handler.saveFromGame();
                logger.log(Level.FINE,"Action: Save game from options menu");
            }
        });
        b3.setOnMouseClicked(
        new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent me)
            {
                System.exit(0);
            }
        });  
        
        tmp.setOnMouseClicked(
        new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent me)
            {
                handler.superSecretSaveFromGameToDefault();
            }
        });
        
        tmp.setOnMouseEntered(
            new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent me)
                {
                    
                    logger.log(Level.INFO, "Entered secret button to save to default save to make debugging easier ");
                }
            });
         tmp.setOnMouseExited(
            new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent me)
                {
                    logger.log(Level.INFO, "Escaped secret button ");
                }
            });
        
        
        this.setOnKeyPressed((KeyEvent e) -> {
            String code = e.getCode().toString();
            System.out.println("Pressing input "+ code + " in the Options Scene");
            //special cases  
            if ( code.equals("ESCAPE")){
                handler.showOptionsScene(); 
            } 
            //put code here, if you want to go from menu to inventory by pressing "I" or something similar
        });
    }
    
    
    
    
}
