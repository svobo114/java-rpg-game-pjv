

package View;

import Controller.MainController;
import Model.Entities.Individual.Chest;
import Model.Entities.PictureEntity;
import Model.Inventory.Items.Item;
import java.util.List;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Class functioning like a builder for a chess scene used in GUI
 * @author Jan Svoboda-Fel-CVUT
 */
public class ChestScene extends Scene implements InventoryListener{
    private static final Logger logger = Logger.getLogger(ChestScene.class.getName());
    private Background invPanelBg;
    private GameSceneMaster handler;
    private InventoryPanel leftPanel;
    private PictureEntity Chest;
    private MainController c;
    private Chest chest;

    /**
     * Makes a new Chest scene with background,connected to a controller, game scene master a GUI Group, in this case VBox
     * @param invPanelBg Background for the panel
     * @param c Main controller to connect to - to give player an item
     * @param handler game scene master - to switch with other scenes inside gameViewMaster
     * @param box Vertical box to set the layout
     */
    public ChestScene(Background invPanelBg,MainController c, GameSceneMaster handler,VBox box){
        super(box);
        this.invPanelBg = invPanelBg;
        this.handler = handler;
        leftPanel=new InventoryPanel(this);
        this.c=c;
        
        box.setSpacing(40);
        Text tmp =new Text("Chest - Click To Take");
        tmp.setFont(Font.font("times", 40));
        tmp.setFill(Color.WHITESMOKE);
        box.getChildren().addAll(tmp,leftPanel);
        box.setBackground(invPanelBg);
        box.setAlignment(Pos.CENTER);
        
         this.setOnKeyPressed((KeyEvent e) -> {
            String code = e.getCode().toString();
            System.out.println("Pressing input "+ code + " in the Chest Scene");
            //special cases  
            if ( code.equals("E")){
                handler.showChestScene(); 
            } 
            if ( code.equals("I")){
                handler.showInventoryScene();
            } 
            if ( code.equals("ESCAPE")){
                handler.showOptionsScene();
            } 

        });
    }
    
    /**
     * Load inventory from another chest
     * @param ch chest to load from
     */
    public void changeInv(Chest ch){
        chest=ch;
        refresh();
    }


    @Override
    public void handleItemClick(Item item, boolean inInv) {
        c.givePlayerItem(item);
        chest.removeItem(item);
        refresh();  
    }
    
    /**
     * Refresh inventory currently shown
     */
    public void refresh(){
        showInv(chest.getInventory());
    }
    
  
    private void showInv(List<Item> inv){
        leftPanel.showInventory(inv);
    }

}
