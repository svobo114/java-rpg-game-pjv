

package View;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;

/**
 *  Class for holding animation data from ONE animation
 * @author Jan Svoboda-Fel-CVUT
 */
public class AnimatedImage{
    private static final Logger logger = Logger.getLogger(AnimatedImage.class.getName());
    private Image[] frames;
    private String name;
    private double duration;
    private double durationAll;
    private boolean overWriteble=true;

    /**
     * Makes a new animation based on length, frames location in resources and priorite
     * @param numFrames how many frames does the animation have
     * @param duration how long should the animation take
     * @param locationWithName full location inside resources
     * @param overwrite if this animation has a low priority and is therefore overwritable
     */
    public AnimatedImage(int numFrames,double duration,String locationWithName,boolean overwrite) {
        this.duration=duration;
        name=locationWithName;
        durationAll=duration*numFrames;
        this.overWriteble=overwrite;
        
        Image[] imageArray = new Image[numFrames];
        for (int i = 0; i < numFrames; i++){
            logger.log(Level.FINE, "fetching a pic from: "+locationWithName + (i+1) + ".png");
            try{
                imageArray[i] = new Image( locationWithName + (i+1) + ".png" );
            }catch (Exception e){
                logger.log(Level.SEVERE, "error fetching animation frames {0} ", e.getMessage());
            }
        }
        frames = imageArray;
    }
     
    /**
     * gets you the current frame based on the timestamp
     * @param time timestamp I want a frame for
     * @return Image of current animation frame
     */
    public Image getFrame(double time) //time since the beginning of animation
    {
        int index = (int)((time % durationAll) / duration);
        //logger.log(Level.INFO, "Inside Image - time: "+time+ ", durationAll " + durationAll+ ", returning frame: "+ index );
        return frames[index];
    }
    
    
    
    /*
    USAGE
        AnimatedImage ufo = new AnimatedImage();
        Image[] imageArray = new Image[6];
        for (int i = 0; i < 6; i++)
            imageArray[i] = new Image( "ufo_" + i + ".png" );
        ufo.frames = imageArray;
        ufo.duration = 0.100;
    */

    /**
     * gets you the length of overall animation
     * @return length of animation in sec
     */
    public double getDurationAll() {
        return durationAll;
    }

    /**
     * gets you boolean meaning this is overwritable = has lower priority
     * @return true if this animation is overwritable
     */
    public boolean isOverWriteble() {
        return overWriteble;
    }

    @Override
    public String toString() {
        return "AnimImg: "+name;
    }
}
