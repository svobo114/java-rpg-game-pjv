

package View;

import Controller.MainController;
import Misc.Coord;
import Misc.Hitbox;
import Model.Entities.Entity;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;


/**
 * Class extending thread that uses AnimationTimer to update entity sprites
 * @author Jan Svoboda-Fel-CVUT
 */
public class GraphicsUpdateThread extends Thread {
    private static final Logger logger = Logger.getLogger(GraphicsUpdateThread.class.getName());
    //map positioning const
    private final static String _MAP_PATH = "map/map_placeholder.png";   //map/map2.png;
    private final static int _WINDOW_W=1179;
    private final static int _WINDOW_H=661;
    private final static int _PLAYER_SPRITE_SIZE=64; //it is symmetrical
    private final static int _HBAR_THICKNESS=8; 
    private final static int _HBAR_OFFSET=16; 
    private final static int _HBAR_CORNERS=2; 
    private final static int _GUI_SCALE=2; 
    private final static Color colH=Color.CRIMSON;
    private final static Color colM=Color.BLUE;
    private final static Color colG=Color.GOLD;
    
    private Image map;      
    private GraphicsContext gc;     
    private long startNanoTime=0;    //in ns
    
    /** Coordinate of the window inside the map */
    private Coord windowCoord;
    /** Coordinate of player on the screen */
    private Coord playerScreenCoord;
    private Coord entityScreenCoord=new Coord(0,0);
    private Hitbox tmpEntityHitbox;
    private Image tmpEntImg;
    
    //entity lists
    private List<Entity> listEntity; 
    private MainController controller;
    
    
    /**
     * Makes a thread that updates graphics
     * @param gc graphicsContext of a canvas that graphics draws to
     * @param controller controller that holds data about players position and hitbox
     * @param listEntity List of entities on the map
     */
    public GraphicsUpdateThread( GraphicsContext gc,MainController controller,List<Entity>listEntity) {
        this.controller=controller;
        this.gc=gc;         //for drawing graphics
        this.listEntity=listEntity;
        tmpEntityHitbox=new Hitbox();
        gc.setFill(colH);
        
        //Loading map
        try{
            map = new Image(_MAP_PATH);
            logger.log(Level.FINEST,"map loaded");
        }catch (Exception e){
            logger.log(Level.SEVERE, "Unable to load some Resource for Graphics Thread, Shutting down: " + e.toString());
            System.exit(-1);
        }
    }
    
    @Override
    public void run(){
        startNanoTime = System.nanoTime();
        new AnimationTimer()
        {
            @Override
            public void handle(long currentNanoTime){
                double time = (currentNanoTime - startNanoTime) / 1000000000.0; 
                
                windowCoord=controller.getWindowCoord();     
                playerScreenCoord=controller.getPlayerScreenCoord();
                gc.drawImage​(map, windowCoord.x, windowCoord.y, _WINDOW_W, _WINDOW_H, 0, 0,_WINDOW_W ,_WINDOW_H ); //0 0 stand for position on canvas
                gc.drawImage(controller.getPlayerSprite(time),playerScreenCoord.x,playerScreenCoord.y,_PLAYER_SPRITE_SIZE,_PLAYER_SPRITE_SIZE);
                
                //calculate entity on the window pos.
                for (Entity ent : listEntity) {
                    tmpEntityHitbox=ent.getHitbox();
                    entityScreenCoord.x=tmpEntityHitbox.leftUp.x-windowCoord.x;
                    entityScreenCoord.y=tmpEntityHitbox.leftUp.y-windowCoord.y;
                    tmpEntImg=ent.whatFrame(time);
                    gc.drawImage(tmpEntImg, 0, 0, tmpEntityHitbox.width,tmpEntityHitbox.height, entityScreenCoord.x, entityScreenCoord.y,tmpEntityHitbox.width, tmpEntityHitbox.height);
                    gc.fillRoundRect(entityScreenCoord.x, entityScreenCoord.y-_HBAR_OFFSET, ent.getHealthPercentage(), _HBAR_THICKNESS, _HBAR_CORNERS,  _HBAR_CORNERS); //drawing the mobs health
                }
                
                drawBars(); //draws over everything
                
            }
        }.start();
    }
    
    
    /**
     * Draws the players health and mana bars + amount of money
     */
    private void drawBars(){ 
        //Mana
        gc.setFill(colM); 
        gc.setStroke(colM);
        gc.fillRoundRect(_HBAR_OFFSET, 2*_HBAR_OFFSET , controller.getPlayerManaPercentage()*_GUI_SCALE, _HBAR_THICKNESS, _HBAR_CORNERS,  _HBAR_CORNERS);
        gc.strokeText(Integer.toString(controller.getPlayerCurrentMana()), 3*_HBAR_OFFSET, 4*_HBAR_OFFSET,_HBAR_OFFSET);//last one is space to occupy
        //Money               
        gc.setStroke(colG); 
        gc.strokeText(Integer.toString(controller.getPlayerMoney()), 5*_HBAR_OFFSET, 4*_HBAR_OFFSET,_HBAR_OFFSET);//last one is space to occupy
        //Back to health
        gc.setFill(colH); 
        gc.setStroke(colH);
        gc.fillRoundRect(_HBAR_OFFSET, _HBAR_OFFSET, controller.getPlayerHealthPercentage()*_GUI_SCALE, _HBAR_THICKNESS, _HBAR_CORNERS,  _HBAR_CORNERS);
        gc.strokeText(Integer.toString(controller.getPlayerCurrentHealth()), _HBAR_OFFSET, 4*_HBAR_OFFSET,_HBAR_OFFSET);//last one is space to occupy
    }
}
