package Misc;

import java.io.Serializable;
import static java.lang.Integer.max;
import static java.lang.Integer.min;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class holding the data about hitbox (2 points or 1 point and width+height)and the max span of where the hitbox can move
 * @author Jan Svoboda-Fel-CVUT
 */
public class Hitbox implements Serializable{
    protected static final Logger logger = Logger.getLogger(Hitbox.class.getName());
    /** left upper corner point of the hitbox*/
    public Coord leftUp;
    /**Right lower corners point of the hitbox*/
    public Coord rightDown;
    private int maxX;
    private int maxY;
    /** width of the hitbox*/
    public int width;
    /**height of the hitbox*/
    public int height;

    /**
     *  Makes the best possible hitbox based on coordinates
     * @param first First point, generally left upper, but does not need to be
     * @param second second point, generally right lower, but does not need to be
     * @param maxX Maximum bound for x coordinate
     * @param maxY maximum bound for y coordinate
    */
    public Hitbox(Coord first, Coord second,int maxX,int maxY) {
        if(first.x<0 || second.x<0 || first.y<0 || second.y<0){
            logger.log(Level.SEVERE, "Error in hitbox: tried assigning negative coordinates");
        }
        this.leftUp = new Coord(min(first.x,second.x),min(first.y,second.y));
        this.rightDown = new Coord(max(first.x,second.x),max(first.y,second.y));
        this.maxX=maxX;
        this.maxY=maxY;
        width=rightDown.x-leftUp.x;
        height = rightDown.y-leftUp.y;
    }
    
    /**
     * Makes the hitbox based on the left upper coord and width+height
     * @param onePoint left upper point of the new hitbox
     * @param maxX Maximum bound for x coordinate
     * @param maxY maximum bound for y coordinate
     * @param width width of new hitbox (x)
     * @param height height of new hitbox (y)
     */
    public Hitbox(Coord onePoint,int maxX, int maxY, int width, int height) {
        if(onePoint.x<0 || onePoint.x<0 ){
            logger.log(Level.SEVERE, "Error in hitbox: tried assigning negative coordinates");
        }
        this.leftUp = onePoint.copy();
        this.rightDown = new Coord(leftUp.x+Math.abs(width),leftUp.y+Math.abs(height));
        this.width=width;
        this.height=height;
        this.maxX=maxX;
        this.maxY=maxY;
    }

    /**
     * Makes a new hitbox that is not on the map. possible for entities without hitboxes?
     */
    public Hitbox() {
        leftUp=new Coord(-1,-1);
        rightDown=leftUp;
        maxX=0;
        maxY=0;
        width=0;
        height=0;
    }

    /**
     * Makes a hitbox from just one point and width+height , and does not assign the maximum boundaries
     * @param onePoint left upper point of the new hitbox
     * @param width width of new hitbox (x)
     * @param height height of new hitbox (y)
     */
    public Hitbox(Coord onePoint, int width, int height) {
        if(onePoint.x<0 || onePoint.x<0 ){
            logger.log(Level.SEVERE, "Error in hitbox: tried assigning negative coordinates");
        }
        this.leftUp = onePoint.copy();
        this.rightDown = new Coord(onePoint.x+Math.abs(width),onePoint.y+Math.abs(height));
        this.leftUp = onePoint;
        this.width = width;
        this.height = height;
        maxX=0;
        maxY=0;
    } 
    
   
    
    
    /**
     * Is this point inside the hitbox?
     * @param point -the point being compared to the hitbox
     * @return true, if the point given is INSIDE the hitbox
     */
    public boolean isIn(Coord point){
        return (    this.leftUp.x<=point.x      && //left upper is more to the left and higher
                    this.leftUp.y<=point.y      &&
                    this.rightDown.x>=point.x   && //right lower is more to the right and lower
                    this.rightDown.y>=point.y   ); 
        
    }
    
    /**
     * Computes, whether two hitboxes collide
     * @param hitbox the second hitbox, that is being compared to this one
     * @return true, if the two hitboxes collide
     */
    public boolean doesCollide(Hitbox hitbox){
        //both hitboxes are rectangles
        return (    this.leftUp.x<=hitbox.rightDown.x        &&
                    this.rightDown.x>=hitbox.leftUp.x        &&
                    this.leftUp.y<=hitbox.rightDown.y        &&
                    this.rightDown.y>=hitbox.leftUp.y        ); 
    }
    
     
    /**
     * Looks, if the hitbox is in range of point
     * @param center the center from which the distance is being measured
     * @param range the range being measured
     * @return true if any of the hitbox's points are in range of the point
     */
    public boolean isInRange(Coord center,int range){
        //will be in range, if at least one corner point is in
        // we have left upper, right lower points. I need the two other
        Coord leftDown= new Coord(leftUp.x,rightDown.y);
        Coord rightUp= new Coord(rightDown.x,leftUp.y);
        return (
                leftUp      .distanceFrom(center)<=range     ||
                leftDown    .distanceFrom(center)<=range     ||
                rightUp     .distanceFrom(center)<=range     ||
                rightDown   .distanceFrom(center)<=range     );
        
    }
    
    
    
    /**
     * Moves the hitbox in the direction of the velocity
     * @param vel has 2 components and move is on each one separately
     */
    public void move (Velocity vel) {
        this.moveX(vel);
        this.moveY(vel);
    }
    
    /**
     * move the hitbox according just to the x component of the velocity
     * @param vel - velocity that moves the hitbox
     */
    public void moveX (Velocity vel) {
        leftUp.x=Integer.min(Integer.max(0,leftUp.x+vel.x), maxX-width);
        rightDown.x=Integer.min(Integer.max(0+width, rightDown.x+vel.x), maxX);
    }
        
    /**
     * move the hitbox according just to the y component of the velocity
     * @param vel - velocity that moves the hitbox
     */
    public void moveY (Velocity vel) {
        leftUp.y=Integer.min(Integer.max(0, leftUp.y+vel.y), maxY-height);
        rightDown.y=Integer.min(Integer.max(0+height, rightDown.y+vel.y), maxY);
    }
    


    /**
     * Copy function that returns a new object that has the same properties
     * @return hitbox copy
     */
    public Hitbox returnCopy(){
        return new Hitbox(this.leftUp.copy(),this.maxX,this.maxY,this.width,this.height);
    }
    
    /**
     * copy function that copies the coords into this hitbox
     * @param HB hitbox to be copied FROM
     */
    public void copyCoords(Hitbox HB){
        this.leftUp=HB.leftUp.copy();
        this.rightDown=HB.rightDown.copy();
    }
    
    /**
     * Change coordinates based on new x of left upper
     * @param x the new X coord of left upper point
     */
    public void setX(int x){
        int tmp = x-leftUp.x;   //diff in x
        leftUp.x=x;             //left up gets the coord directly
        rightDown.x+=tmp;       //add the diff to RL to move it with UL
    }
    

    /**
     * Change coordinates based on new x of left upper
     * @param y the new y coord of left upper point
     */
    public void setY(int y){
        int tmp =y-leftUp.y;
        leftUp.y=y;
        rightDown.y+=tmp;
    }
    
    
}
