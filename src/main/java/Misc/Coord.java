

package Misc;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class holding the data about coordinates and the max span of those
 * @author Jan Svoboda-Fel-CVUT
 */
public class Coord implements Serializable {
    private static final Logger logger = Logger.getLogger(Coord.class.getName());
    /** X component of a coordinate*/
    public int x;
    /**Y coordinate of a coordinate */
    public int y;


    /**
     * makes coord with x,y integers
     * @param x new x coord
     * @param y new y coord
     */
    public Coord(int x, int y) {
        this.x = x;
        this.y = y;

    }
 
    /**
     * Change x,y from outside function. useful when moving
     * @param x new coord x
     * @param y new coord y
     */
    public void setCoord(int x, int y){
        this.x=x;
        this.y=y;
    }


    /**
     * Copy method to return a new object with the same propertioes
     * @return Coord instance which is a copy of this coord
     */
    public Coord copy() {
        return new Coord(this.x,this.y);
    }

    /**
     * Calculates a distance froma rgument coordinate
     * @param point to calculate the distance from
     * @return int specifying a distane betwen coords
     */
    public int distanceFrom(Coord point){
        int xDiff=this.x-point.x;
        int yDiff=this.y-point.y;
        int dist=(int)Math.floor(Math.sqrt(xDiff*xDiff+yDiff*yDiff));
        logger.log(Level.FINE, "distance of point: ({0},{1}) from this point: ({2},{3}) is: {4}", new Object[]{point.x, point.y, x, y, dist});
        return dist ;
    }
    
     /**
     *  returns true, if points are the same.
     *  also works for absolute coord, you can have different orientation and still be the same
     * @return    true of the two coords have equal Xs and Ys
     * @param another the coord that is being compared to
    */ 
    public boolean equals(Coord another){
        return this.x==another.x && this.y==another.y;
    }

}
