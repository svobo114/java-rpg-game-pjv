

package Misc;

import java.io.Serializable;
import java.util.logging.Logger;

/**
 * Small class holding data about movement
 * @author Jan Svoboda-Fel-CVUT
 */
public class Velocity implements Serializable{
    private static final Logger logger = Logger.getLogger(Velocity.class.getName());
    /** x component of velocity*/
    public int x;
    /** y component of velocity*/
    public int y;

    /**
     * construct a velocity from x and y components
     * @param velocityX - X component of movement
     * @param velocityY - Y compinent of movement
     */
    public Velocity(int velocityX, int velocityY) {
        this.x = velocityX;
        this.y = velocityY;
    }

    /**
     * def. constructor
     */
    public Velocity() {
    }
    
    /**
     * zero the velocity
     */
    public void zero(){
        x=0;
        y=0;        
    }
    
}
