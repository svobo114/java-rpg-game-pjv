
Simplified RPG Game in java using javafx.
Semestral project for class B0B36-PJV Fel CTU in Prague for spring semester 2023.
Project Specifications:
    https://cw.fel.cvut.cz/b212/courses/b0b36pjv/semestral/start
    https://cw.fel.cvut.cz/b212/courses/b0b36pjv/semestral/herni_engine

The Idea is to make an engine, and make a short game showcasing the functionality.

Author: Jan Svoboda, OI FEL CVUT
